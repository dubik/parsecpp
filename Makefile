last:
	ant compile
	cp nsinit pax/pa8/
	cd pax/pa8/ && make test

pa7:
	ant compile
	cp nsdecl pax/pa7/
	cd pax/pa7/ && make test

pa7_tc:
	cp nsdecl pax/pa7/
	cd pax/pa7/ && scripts/run_all_tests_tc.pl nsdecl ref my tests
