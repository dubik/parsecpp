package org.dubikdev.cpp.lexer;

%%

%public
%line
%type Token
%class CppLexer

ALPHA=[A-Za-z]
DIGIT=[0-9]
NEWLINE=\r|\n|\r\n
WHITE_SPACE_CHAR=[\n\r\ \t]
Ident = {ALPHA}({ALPHA}|{DIGIT}|_)*
STRING = \".*\"

%%
    "unsigned char" { return Token.kw(ETokenType.KW_UNSIGNED_CHAR); }
    "signed char" { return Token.kw(ETokenType.KW_SIGNED_CHAR); }
    "unsigned int" { return Token.kw(ETokenType.KW_UNSIGNED_INT); }
    "signed int" { return Token.kw(ETokenType.KW_INT); }
    "unsigned short int" { return Token.kw(ETokenType.KW_UNSIGNED_SHORT_INT); }
    "unsigned short" { return Token.kw(ETokenType.KW_UNSIGNED_SHORT_INT); }
    "unsigned long int" { return Token.kw(ETokenType.KW_UNSIGNED_LONG_INT); }
    "unsigned long" { return Token.kw(ETokenType.KW_UNSIGNED_LONG_INT); }
    "unsigned long long int" { return Token.kw(ETokenType.KW_UNSIGNED_LONG_LONG_INT); }
    "unsigned long long" { return Token.kw(ETokenType.KW_UNSIGNED_LONG_LONG_INT); }
    "signed long int" { return Token.kw(ETokenType.KW_LONG_INT); }
    "signed long" { return Token.kw(ETokenType.KW_LONG_INT); }
    "signed long long int" { return Token.kw(ETokenType.KW_LONG_LONG_INT); }
    "signed long long" { return Token.kw(ETokenType.KW_LONG_LONG_INT); }
    "long long int" { return Token.kw(ETokenType.KW_LONG_LONG_INT); }
    "long int" { return Token.kw(ETokenType.KW_LONG_INT); }
    "signed short int" { return Token.kw(ETokenType.KW_SHORT_INT); }
    "signed short" { return Token.kw(ETokenType.KW_SHORT_INT); }
    "short int" { return Token.kw(ETokenType.KW_SHORT_INT); }
    "long double" { return Token.kw(ETokenType.KW_LONG_DOUBLE); }

    "alignas" { return Token.kw(ETokenType.KW_ALIGNAS); }
    "alignof" { return Token.kw(ETokenType.KW_ALIGNOF); }
    "asm" { return Token.kw(ETokenType.KW_ASM); }
    "auto" { return Token.kw(ETokenType.KW_AUTO); }
    "bool" { return Token.kw(ETokenType.KW_BOOL); }
    "break" { return Token.kw(ETokenType.KW_BREAK); }
    "case" { return Token.kw(ETokenType.KW_CASE); }
    "catch" { return Token.kw(ETokenType.KW_CATCH); }
    "char" { return Token.kw(ETokenType.KW_CHAR); }
    "char16_t" { return Token.kw(ETokenType.KW_CHAR16_T); }
    "char32_t" { return Token.kw(ETokenType.KW_CHAR32_T); }
    "class" { return Token.kw(ETokenType.KW_CLASS); }
    "const" { return Token.kw(ETokenType.KW_CONST); }
    "constexpr" { return Token.kw(ETokenType.KW_CONSTEXPR); }
    "const_cast" { return Token.kw(ETokenType.KW_CONST_CAST); }
    "continue" { return Token.kw(ETokenType.KW_CONTINUE); }
    "decltype" { return Token.kw(ETokenType.KW_DECLTYPE); }
    "default" { return Token.kw(ETokenType.KW_DEFAULT); }
    "delete" { return Token.kw(ETokenType.KW_DELETE); }
    "do" { return Token.kw(ETokenType.KW_DO); }
    "double" { return Token.kw(ETokenType.KW_DOUBLE); }
    "dynamic_cast" { return Token.kw(ETokenType.KW_DYNAMIC_CAST); }
    "else" { return Token.kw(ETokenType.KW_ELSE); }
    "enum" { return Token.kw(ETokenType.KW_ENUM); }
    "explicit" { return Token.kw(ETokenType.KW_EXPLICIT); }
    "export" { return Token.kw(ETokenType.KW_EXPORT); }
    "extern" { return Token.kw(ETokenType.KW_EXTERN); }
    "false" { return Token.kw(ETokenType.KW_FALSE); }
    "float" { return Token.kw(ETokenType.KW_FLOAT); }
    "for" { return Token.kw(ETokenType.KW_FOR); }
    "friend" { return Token.kw(ETokenType.KW_FRIEND); }
    "goto" { return Token.kw(ETokenType.KW_GOTO); }
    "if" { return Token.kw(ETokenType.KW_IF); }
    "inline" { return Token.kw(ETokenType.KW_INLINE); }
    "int" { return Token.kw(ETokenType.KW_INT); }
    "long" { return Token.kw(ETokenType.KW_LONG_INT); }
    "mutable" { return Token.kw(ETokenType.KW_MUTABLE); }
    "namespace" { return Token.kw(ETokenType.KW_NAMESPACE); }
    "new" { return Token.kw(ETokenType.KW_NEW); }
    "noexcept" { return Token.kw(ETokenType.KW_NOEXCEPT); }
    "nullptr" { return Token.kw(ETokenType.KW_NULLPTR); }
    "operator" { return Token.kw(ETokenType.KW_OPERATOR); }
    "private" { return Token.kw(ETokenType.KW_PRIVATE); }
    "protected" { return Token.kw(ETokenType.KW_PROTECTED); }
    "public" { return Token.kw(ETokenType.KW_PUBLIC); }
    "register" { return Token.kw(ETokenType.KW_REGISTER); }
    "reinterpret_cast" { return Token.kw(ETokenType.KW_REINTERPET_CAST); }
    "return" { return Token.kw(ETokenType.KW_RETURN); }
    "short" { return Token.kw(ETokenType.KW_SHORT_INT); }
    "signed" { return Token.kw(ETokenType.KW_INT); }
    "sizeof" { return Token.kw(ETokenType.KW_SIZEOF); }
    "static" { return Token.kw(ETokenType.KW_STATIC); }
    "static_assert" { return Token.kw(ETokenType.KW_STATIC_ASSERT); }
    "static_cast" { return Token.kw(ETokenType.KW_STATIC_CAST); }
    "struct" { return Token.kw(ETokenType.KW_STRUCT); }
    "switch" { return Token.kw(ETokenType.KW_SWITCH); }
    "template" { return Token.kw(ETokenType.KW_TEMPLATE); }
    "this" { return Token.kw(ETokenType.KW_THIS); }
    "thread_local" { return Token.kw(ETokenType.KW_THREAD_LOCAL); }
    "throw" { return Token.kw(ETokenType.KW_THROW); }
    "true" { return Token.kw(ETokenType.KW_TRUE); }
    "try" { return Token.kw(ETokenType.KW_TRY); }
    "typedef" { return Token.kw(ETokenType.KW_TYPEDEF); }
    "typeid" { return Token.kw(ETokenType.KW_TYPEID); }
    "typename" { return Token.kw(ETokenType.KW_TYPENAME); }
    "union" { return Token.kw(ETokenType.KW_UNION); }
    "unsigned" { return Token.kw(ETokenType.KW_UNSIGNED_INT); }
    "using" { return Token.kw(ETokenType.KW_USING); }
    "virtual" { return Token.kw(ETokenType.KW_VIRTUAL); }
    "void" { return Token.kw(ETokenType.KW_VOID); }
    "volatile" { return Token.kw(ETokenType.KW_VOLATILE); }
    "wchar_t" { return Token.kw(ETokenType.KW_WCHAR_T); }
    "while" { return Token.kw(ETokenType.KW_WHILE); }
    "{" { return Token.kw(ETokenType.OP_LBRACE); }
    "<%" { return Token.kw(ETokenType.OP_LBRACE); }
    "}" { return Token.kw(ETokenType.OP_RBRACE); }
    "%>" { return Token.kw(ETokenType.OP_RBRACE); }
    "[" { return Token.kw(ETokenType.OP_LSQUARE); }
    "<:" { return Token.kw(ETokenType.OP_LSQUARE); }
    "]" { return Token.kw(ETokenType.OP_RSQUARE); }
    ":>" { return Token.kw(ETokenType.OP_RSQUARE); }
    "(" { return Token.kw(ETokenType.OP_LPAREN); }
    ")" { return Token.kw(ETokenType.OP_RPAREN); }
    "|" { return Token.kw(ETokenType.OP_BOR); }
    "bitor" { return Token.kw(ETokenType.OP_BOR); }
    "^" { return Token.kw(ETokenType.OP_XOR); }
    "xor" { return Token.kw(ETokenType.OP_XOR); }
    "~" { return Token.kw(ETokenType.OP_COMPL); }
    "compl" { return Token.kw(ETokenType.OP_COMPL); }
    "&" { return Token.kw(ETokenType.OP_AMP); }
    "bitand" { return Token.kw(ETokenType.OP_AMP); }
    "!" { return Token.kw(ETokenType.OP_LNOT); }
    "not" { return Token.kw(ETokenType.OP_LNOT); }
    ";" { return Token.kw(ETokenType.OP_SEMICOLON); }
    ":" { return Token.kw(ETokenType.OP_COLON); }
    "..." { return Token.kw(ETokenType.OP_DOTS); }
    "?" { return Token.kw(ETokenType.OP_QMARK); }
    "::" { return Token.kw(ETokenType.OP_COLON2); }
    "." { return Token.kw(ETokenType.OP_DOT); }
    ".*" { return Token.kw(ETokenType.OP_DOTSTAR); }
    "+" { return Token.kw(ETokenType.OP_PLUS); }
    "-" { return Token.kw(ETokenType.OP_MINUS); }
    "*" { return Token.kw(ETokenType.OP_STAR); }
    "/" { return Token.kw(ETokenType.OP_DIV); }
    "%" { return Token.kw(ETokenType.OP_MOD); }
    "=" { return Token.kw(ETokenType.OP_ASS); }
    "<" { return Token.kw(ETokenType.OP_LT); }
    ">" { return Token.kw(ETokenType.OP_GT); }
    "+=" { return Token.kw(ETokenType.OP_PLUSASS); }
    "-=" { return Token.kw(ETokenType.OP_MINUSASS); }
    "*=" { return Token.kw(ETokenType.OP_STARASS); }
    "/=" { return Token.kw(ETokenType.OP_DIVASS); }
    "%=" { return Token.kw(ETokenType.OP_MODASS); }
    "^=" { return Token.kw(ETokenType.OP_XORASS); }
    "xor_eq" { return Token.kw(ETokenType.OP_XORASS); }
    "&=" { return Token.kw(ETokenType.OP_BANDASS); }
    "and_eq" { return Token.kw(ETokenType.OP_BANDASS); }
    "|=" { return Token.kw(ETokenType.OP_BORASS); }
    "or_eq" { return Token.kw(ETokenType.OP_BORASS); }
    "<<" { return Token.kw(ETokenType.OP_LSHIFT); }
    ">>" { return Token.kw(ETokenType.OP_RSHIFT); }
    ">>=" { return Token.kw(ETokenType.OP_RSHIFTASS); }
    "<<=" { return Token.kw(ETokenType.OP_LSHIFTASS); }
    "==" { return Token.kw(ETokenType.OP_EQ); }
    "!=" { return Token.kw(ETokenType.OP_NE); }
    "not_eq" { return Token.kw(ETokenType.OP_NE); }
    "<=" { return Token.kw(ETokenType.OP_LE); }
    ">=" { return Token.kw(ETokenType.OP_GE); }
    "&&" { return Token.kw(ETokenType.OP_LAND); }
    "and" { return Token.kw(ETokenType.OP_LAND); }
    "||" { return Token.kw(ETokenType.OP_LOR); }
    "or" { return Token.kw(ETokenType.OP_LOR); }
    "++" { return Token.kw(ETokenType.OP_INC); }
    "--" { return Token.kw(ETokenType.OP_DEC); }
    "," { return Token.kw(ETokenType.OP_COMMA); }
    "->*" { return Token.kw(ETokenType.OP_ARROWSTAR); }
    "->" { return Token.kw(ETokenType.OP_ARROW); }

    {Ident} { return new Token(ETokenType.TY_ID, yytext()); }
    ({DIGIT})+ { return new Token(ETokenType.TY_LITERAL, yytext()); }
    {STRING} { return new Token(ETokenType.TY_LITERAL, yytext()); }
    {WHITE_SPACE_CHAR} { /* ignore */}
    {NEWLINE} { /* ignore */}

. { throw new IllegalArgumentException("Can't recognize: " + yytext()); }