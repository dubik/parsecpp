package org.dubikdev.cpp

import org.junit.Test
import org.junit.Assert._

class StaticAssertTest extends NSInitBaseTest {
  @Test
  def intVarStaticAssert() = {
    expectException {
      parse_simple_declaration("int x = 3;\nstatic_assert(x, \"not constexpr\");")
    }
  }

  @Test
  def constIntVarStaticAssert() = {
    assertEquals("50 41 38 00 03 00 00 00", dumpToHex("const int x = 3;\nstatic_assert(x, \"\");"))
  }

  @Test
  def constExprPointerInitializedWithArray() = {
    assertEquals("50 41 38 00 00 00 00 00 04 00 00 00 00 00 00 00",
      dumpToHex("char a[3];\nconstexpr const char* p = a;\nstatic_assert(p, \"null pointer\");"))
  }
}
