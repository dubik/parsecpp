package org.dubikdev.cpp

import org.dubikdev.cpp.semantic._
import org.dubikdev.cpp.EntryFactory._
import scala.collection.mutable.ListBuffer

class TestVisitor extends EntityVisitor {
  val entries = ListBuffer[Entry]()

  def visit(entity: VariableEntity): Unit = {
    val name = entity.name
    val typeStr = entity.varType.toString
    entries += variable(s"$name $typeStr")
  }

  def visit(entity: FunctionEntity): Unit = {
    entries += func(entity.name + " " + entity.funcType.toString)
  }

  def visit(entity: TypedefEntity): Unit = {
    entries += typedef(entity.name + " " + entity.typedefType.toString)
  }

  def visitEnter(entity: NamespaceEntity): Unit = {
    entries += startNS(entity.name)
    if (entity.inline)
      entries += inlineNS()
  }

  def visitExit(entity: NamespaceEntity): Unit = entries += endNS()
}
