package org.dubikdev.cpp

import org.junit.Assert._
import org.dubikdev.cpp.EntryFactory._
import org.junit.{After, Before, Ignore, Test}
import org.dubikdev.cpp.support.PA

class TypedefParserTest extends ParserBaseTest {
  @Before
  def setUp() = {
    PA.isPA7 = true
  }

  @After
  def tearDown() = {
    PA.isPA7 = false
  }

  @Test
  def testResolutionOperatorForTypedef() {
    val expected = List(startNS(), startNS("C"), variable("x char"), endNS(), typedef("T char"), endNS())
    val actual = parse_and_walk("typedef char T; namespace C {::T x;}")

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def testTypedefForTypedefedType() {
    val expected = List(startNS(), variable("i char"), typedef("T char"), typedef("CT char"), endNS())
    val actual = parse_and_walk("typedef char T; typedef T CT; CT i;")

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def typedefArrayOfInt() {
    val expected = List(startNS(), variable("i array of unknown bound of int"), typedef("UNKA array of unknown bound of int"), endNS())
    val actual = parse_and_walk("typedef int UNKA[]; UNKA i;")

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def typedefConstArrayOfInt() {
    val expected = List(startNS(), variable("p2 pointer to array of 5 const char"), typedef("CA array of 5 char"),
      typedef("arr2 array of 5 const char"), endNS())
    val actual = parse_and_walk("typedef char CA[5]; typedef const CA arr2; arr2* p2;")

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def typedefAndUsing() {
    val expected = List(startNS(), func("g function of (...) returning pointer to function of () returning void"),
      typedef("G function of (...) returning pointer to function of () returning void"), endNS())
    val actual = parse_and_walk(
      """using F = void();
        |typedef F* G(...);
        |G g;""".stripMargin)

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def typedefLvalueReferenceToInt() {
    val expected = List(startNS(), typedef("LRI lvalue-reference to int"), endNS())
    val actual = parse_and_walk("typedef int & LRI;")

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def typedefRvalueReferenceToInt() {
    val expected = List(startNS(), typedef("RRI rvalue-reference to int"), endNS())
    val actual = parse_and_walk("typedef int && RRI;")

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def declOfVarOfConstTypedefLvalueRefToInt() {
    val expected = List(startNS(), variable("i int"), variable("r1 lvalue-reference to int"), typedef("LRI lvalue-reference to int"), endNS())
    val actual = parse_and_walk("typedef int & LRI; int i; const LRI& r1;")


    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def declOfVarOfTypedefLvalueRefRefToInt() {
    val expected = List(startNS(), variable("i int"), variable("r1 lvalue-reference to int"), typedef("LRI lvalue-reference to int"), endNS())
    val actual = parse_and_walk("typedef int & LRI; int i; const LRI&& r1;")

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def pointerToReference() {
    expectException {
      parse_and_walk("typedef int&* T1;")
    }
  }

  @Test
  def referenceToVoid() {
    expectException {
      parse_and_walk("typedef void& T1;")
    }
  }

  @Test
  def referencetoReferenceOfInt() {
    expectException {
      parse_and_walk("typedef int & & T1;")
    }
  }
}
