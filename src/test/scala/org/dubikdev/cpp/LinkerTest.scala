package org.dubikdev.cpp

import org.junit.Test
import org.dubikdev.cpp.semantic._
import EFundamentalType._
import org.dubikdev.cpp.semantic.FundamentalType
import org.dubikdev.cpp.linker.Linker
import org.junit.Assert._
import org.dubikdev.cpp.ast.LiteralExpression
import Specifiers._

class LinkerTest extends NSInitBaseTest {
  @Test
  def testExternalDecl() = {
    val rootNSA = new NamespaceEntity
    // extern int k;
    val externVarDecl = new VariableEntity("k", List(), new FundamentalType(FT_INT), ExternSpecifier, new NoInitializer)
    rootNSA.add(externVarDecl)

    val rootNSB = new NamespaceEntity
    // int k;
    val initializer = new CopyInitializer(new LiteralExpression("3"))
    val varDef = new VariableEntity("k", new FundamentalType(FT_INT), initializer)
    rootNSB.add(varDef)

    val linkedProgram = Linker.linkObjectModels(List(rootNSA, rootNSB))

    assertEquals(1, linkedProgram.size)
    assertTrue(linkedProgram(0).isInstanceOf[VariableEntity])
    assertEquals(initializer, linkedProgram(0).asInstanceOf[VariableEntity].initializer)
  }

  @Test
  def testLinkVarInNamespaceAndNot() = {
    val rootNS = new NamespaceEntity
    rootNS.add(new VariableEntity("x", new FundamentalType(FT_INT), new CopyInitializer(new LiteralExpression("1"))))

    val rootA = new NamespaceEntity
    rootA.add(new VariableEntity("y", new FundamentalType(FT_INT), new CopyInitializer(new LiteralExpression("2"))))

    rootNS.add(rootA)

    val linkedProgram = Linker.linkObjectModels(List(rootNS))
    assertEquals(2, linkedProgram.size)
    assertEquals("x", linkedProgram(0).name)
    assertEquals("y", linkedProgram(1).name)
  }

  @Test
  def constIntVariable() = {
    expectException {
      parse_simple_declaration("const int x; int main() {}")
    }
  }


  @Test
  def testNonEnclosingQualifiedDecl() = {
    val source =
      """
        |namespace A
        |{
        |    extern int x;
        |}
        |namespace B
        |{
        |    int A::x;
        |}
      """.stripMargin

    expectException {
      val context = parse_simple_declaration(source).context
      Linker.linkObjectModels(List(context.defaultNamespace))
    }
  }

  @Test
  def testFunctionRedefinition() = {
    expectException {
      parse_simple_declaration("void f() {}\nvoid f() {}")
    }
  }

  @Test
  def testRedeclarationOfExternConst() = {
    val source =
      """
        |namespace A
        |{
        |	extern const int x;
        |}
        |
        |const int A::x = 3;
      """.stripMargin

    assertEquals("50 41 38 00 03 00 00 00", dumpToHex(source))
  }

  @Test
  def testRedeclarationOfArray() = {
    val source =
      """
        |namespace A
        |{
        |	extern int arr[];
        |	const int y = 3;
        |}
        |
        |int A::arr[y];
      """.stripMargin

    assertEquals("50 41 38 00 00 00 00 00 00 00 00 00 00 00 00 00 03 00 00 00", dumpToHex(source))
  }

  @Test
  def testInitializationOrder() = {
    val source =
      """
        |namespace A
        |{
        |	extern const int x;
        | int k = 7;
        | const int y = 3;
        |}
        |
        |const int A::x = y;
      """.stripMargin

    assertEquals("50 41 38 00 03 00 00 00 07 00 00 00 03 00 00 00", dumpToHex(source))
  }

  @Test
  def testNamespaceAlias() = {
    val source =
      """
        |namespace std
        |{
        |	using size_t = unsigned long int;
        |}
        |
        |const std::size_t v = 42;
      """.stripMargin

    assertEquals("50 41 38 00 00 00 00 00 2A 00 00 00 00 00 00 00", dumpToHex(source))
  }

  @Test
  def testRefToVar() = {
    val source =
      """
        |namespace std
        |{
        |	using size_t = unsigned long int;
        |}
        |
        |const std::size_t v = 42;
        |
        |const std::size_t& x = v;
      """.stripMargin

    assertEquals("50 41 38 00 00 00 00 00 2A 00 00 00 00 00 00 00 08 00 00 00 00 00 00 00", dumpToHex(source))
  }

  @Test
  def testRefToRefToVar() = {
    val source =
      """
        |namespace std
        |{
        |	using size_t = unsigned long int;
        |}
        |
        |const std::size_t v = 42;
        |const std::size_t& x = v;
        |const std::size_t& y = x;
      """.stripMargin


    assertEquals("50 41 38 00 00 00 00 00 2A 00 00 00 00 00 00 00 08 00 00 00 00 00 00 00 08 00 00 00 00 00 00 00", dumpToHex(source))
  }
}
