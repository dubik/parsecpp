package org.dubikdev.cpp

import org.dubikdev.cpp.abi.PA8MockDumper
import scala.collection.mutable.ListBuffer
import org.dubikdev.cpp.linker.Linker
import org.junit.Assert._
import scala.collection.mutable
import org.dubikdev.cpp.semantic.NamedEntity

trait NSInitBaseTest extends ParserBaseTest {
  def parse_and_dump(source: String): List[Byte] = {
    val context = parse_simple_declaration(source).context
    val linkedProgram = Linker.linkObjectModels(List(context.defaultNamespace))

    PA8MockDumper(linkedProgram)
  }

  def toHexString(out: List[Byte]): String =
    out.map("%02X" format _).mkString(" ")

  def dumpToHex(source: String): String = toHexString(parse_and_dump(source))
}
