package org.dubikdev.cpp

import org.dubikdev.cpp.semantic.{FundamentalType, DeclSpecifier}
import org.dubikdev.cpp.lexer.ETokenType._
import org.dubikdev.cpp.lexer.Token._
import org.dubikdev.cpp.semantic.EFundamentalType._
import org.junit.Assert._
import org.junit.Test

class DeclSpecifierTest {
  @Test
  def testSimpleConstIntK() = {
    val declSpecifier = new DeclSpecifier()
    declSpecifier.setTypeSpecifier(new FundamentalType(FT_INT))
    declSpecifier.setCVQualifier(kw(KW_CONST))
    assertEquals("const int", declSpecifier.createBaseType().toString)
  }
}
