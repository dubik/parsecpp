package org.dubikdev.cpp

import org.junit.Test
import org.dubikdev.cpp.semantic.ConstantExpressionEvaluator
import org.dubikdev.cpp.ast.LiteralExpression
import org.junit.Assert._

class ConstantExpressionEvaluatorTest extends {
  @Test
  def testSimpleLiteralExpression() = {
    assertEquals("1", ConstantExpressionEvaluator.calculate(new LiteralExpression("1")))
  }
}
