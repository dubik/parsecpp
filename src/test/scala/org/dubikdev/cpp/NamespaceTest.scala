package org.dubikdev.cpp

import org.dubikdev.cpp.semantic._
import org.junit.Assert._
import EFundamentalType._
import EEntityType._
import org.junit.Test

class NamespaceTest {
  @Test
  def testSimpleLookup() = {
    val ns = new NamespaceEntity()
    val nsA = new NamespaceEntity("A")
    ns.add(nsA)
    val lookupedNS = ns.lookup("A", ET_Namespace)

    assertFalse(lookupedNS.isEmpty)
    assertEquals(nsA, lookupedNS.get)
  }

  @Test
  def testLookupNonExistingItem() = {
    val ns = new NamespaceEntity()
    val lookupedNS = ns.lookup("A", ET_Namespace)
    assertTrue(lookupedNS.isEmpty)
  }

  @Test
  def testLookupTypedef() = {
    val ns = new NamespaceEntity()
    val typedefEntity = new TypedefEntity("TInt", new FundamentalType(FT_INT))
    ns.add(typedefEntity)
    val typeDef = ns.lookup("TInt", ET_Typedef)
    assertFalse(typeDef.isEmpty)
    assertEquals(typedefEntity, typeDef.get)
  }

  @Test
  def testUsingNamespace(): Unit = {
    val rootNS = new NamespaceEntity()
    val a = new NamespaceEntity("A")
    a.add(new TypedefEntity("T", new FundamentalType(FT_INT)))
    val b = new NamespaceEntity("B")
    b.add(new TypedefEntity("T", new FundamentalType(FT_CHAR)))
    rootNS.add(a)
    rootNS.add(b)
    rootNS.attachedNamespaces += a

    rootNS.lookup("T", ET_Typedef) match {
      case Some(x) => {
        assertEquals(x.asInstanceOf[TypedefEntity].typedefType.asInstanceOf[FundamentalType].toString, "int")
      }
      case None => fail("A::T was not found")
    }
  }
}
