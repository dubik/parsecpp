package org.dubikdev.cpp

import org.junit.Test
import org.junit.Assert._
import scala.collection.mutable.ListBuffer

class DumpEntitiesTest extends NSInitBaseTest {
  @Test
  def dumpMagicString() = {
    assertEquals("50 41 38 00", dumpToHex(";"))
  }

  @Test
  def dumpIntVariable() = {
    assertEquals("50 41 38 00 00 00 00 00", dumpToHex("int x;"))
  }

  @Test
  def dumpFunc() = {
    assertEquals("50 41 38 00 66 75 6E 00", dumpToHex("int func() {}"))
  }

  @Test
  def dumpIntVarAndFunc() = {
    assertEquals("50 41 38 00 00 00 00 00 66 75 6E 00", dumpToHex("int x; int main() {}"))
  }

  @Test
  def dumpPointerToFunction() = {
    assertEquals("50 41 38 00 00 00 00 00 00 00 00 00 00 00 00 00", dumpToHex("void (*p)();"))
  }

  @Test
  def dumpVarAndPointerToFunction() = {
    assertEquals("50 41 38 00 66 75 6E 00 00 00 00 00 00 00 00 00", dumpToHex("void f() {}\nvoid (*p)();"))
  }

  @Test
  def dumpArrayOfInts() = {
    assertEquals("50 41 38 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00",
      dumpToHex("int x[10];"))
  }

  @Test
  def dumpPoinerToFuncAssign() = {
    assertEquals("50 41 38 00 66 75 6E 00 04 00 00 00 00 00 00 00", dumpToHex("void f() {}; void (*p)() = f;"))
  }

  @Test
  def dumpInitializedReferenceToConstInt() = {
    assertEquals("50 41 38 00 00 00 00 00 10 00 00 00 00 00 00 00 03 00 00 00", dumpToHex("const int& i = 3;"))
  }
}
