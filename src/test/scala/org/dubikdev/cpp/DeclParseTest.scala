package org.dubikdev.cpp

import EntryFactory._
import org.junit.Assert._
import org.junit.Test

class DeclParseTest extends ParserBaseTest {
  @Test
  def testHandleGlobalOP_COLON2operator() = {
    val expected = List(startNS(), startNS("C"), variable("x char"), endNS(), typedef("T char"), endNS())
    val actual = parse_and_walk("typedef char T; namespace C {::T x;}")
    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def testSearchForTypedefDefinitionInGlobalScope() = {
    val expected = List(startNS(), startNS("C"), variable("x char"), endNS(), typedef("T char"), endNS())
    val actual = parse_and_walk("typedef char T; namespace C {T x;}")
    assertEquals(expected, actual)
  }

  @Test
  def testResolveTypedef() = {
    val expected = List(startNS(), variable("k int"), typedef("TInt int"), endNS())
    val actual = parse_and_walk("typedef int TInt; TInt k;")
    assertEquals(expected, actual)
  }

  @Test
  def testCreateModelOfIntkConstFloat() = {
    val expected = List(startNS(), func("k function of (float) returning int"), endNS())
    val actual = parse_and_walk("int k(const float);")
    assertEquals(expected, actual)
  }

  @Test
  def testCreateModelOfIntk() = {
    val expected = List(startNS(), variable("k pointer to int"), endNS())
    val actual = parse_and_walk("int *k;")
    assertEquals(expected, actual)
  }

  @Test
  def testCreateModelOfConstIntK() = {
    val expected = List(startNS(), variable("k pointer to const int"), endNS())
    val actual = parse_and_walk("const int * k;")
    assertEquals(expected, actual)
  }

  @Test
  def testCreateModelOfFunc() = {
    val expected = List(startNS(), func("k function of (float) returning int"), endNS())
    val actual = parse_and_walk("int k(float);")
    assertEquals(expected, actual)
  }

  @Test
  def testCreateModelOfIntkFloat() = {
    val expected = List(startNS(), variable("k pointer to int"), endNS())
    val actual = parse_and_walk("int *k;")
    assertEquals(expected, actual)
  }

  @Test
  def testCreateModelOfComplexFunc() = {
    val expected = List(startNS(), func("fpif function of (pointer to double) returning pointer to function of (pointer to float) returning int"), endNS())
    val actual = parse_and_walk("int (*fpif(double *))(float *);")
    assertEquals(expected, actual)
  }

  @Test
  def testCreateModelOfIntArray() = {
    val expected = List(startNS(), variable("k array of 5 int"), endNS())
    val actual = parse_and_walk("int k[5];")
    assertEquals(expected, actual)
  }

  @Test
  def testCreateModelOfTypedef() = {
    val expected = List(startNS(), typedef("TInt int"), endNS())
    val actual = parse_and_walk("typedef int TInt;")
    assertEquals(expected, actual)
  }

  @Test
  def testParseEmpty() = {
    val expected = List(startNS(), endNS())
    val actual = parse_and_walk("")
    assertEquals(expected, actual)
  }

  @Test
  def testDefineOneNamespace() = {
    val expected = List(startNS(), startNS("A"), endNS(), endNS())
    val actual = parse_and_walk("namespace A {};")
    assertEquals(expected, actual)
  }

  @Test
  def typedefArrayOfUnknownBoundsButDefineAsArrayOfKnownBounds() = {
    val expected = List(startNS(), variable("arr array of 10 int"), endNS())
    val actual = parse_and_walk("extern int arr[]; int arr[10];")

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def testParsingFuncDeclWithNamedParams() = {
    val expected = List(startNS(), func("k function of (int) returning int"), endNS())
    val actual = parse_and_walk("int k(int p);")

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def testQualifiedVarDeclaration() = {
    val actual = parse_and_walk("int X::k;")
    assertFalse(false)
  }
}
