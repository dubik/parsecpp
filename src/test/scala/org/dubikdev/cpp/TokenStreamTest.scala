package org.dubikdev.cpp

import org.dubikdev.cpp.lexer.Token._
import org.dubikdev.cpp.lexer.ETokenType._
import org.dubikdev.cpp.lexer.CppLexer
import java.io.StringReader
import org.junit.Assert._
import org.junit.Test

class TokenStreamTest {
  @Test
  def testNext() = {
    val stream = new TokenStream(new CppLexer(new StringReader("int k;")))
    assertTrue(stream.hasNext)
    assertEquals(kw(KW_INT), stream.next())
    assertEquals(id("k"), stream.next())
    assertEquals(kw(OP_SEMICOLON), stream.next())
    assertTrue(!stream.hasNext)
  }

  @Test
  def testRestore() = {
    val stream = new TokenStream(new CppLexer(new StringReader("int k;")))
    stream.save()
    stream.next()
    stream.restore()
    assertEquals(kw(KW_INT), stream.next())
  }

  @Test
  def testDiscard() = {
    val stream = new TokenStream(new CppLexer(new StringReader("int k;")))
    stream.save()
    stream.next()
    stream.discard()
    assertEquals(id("k"), stream.next())
  }

  @Test
  def testRequired() = {
    val stream = new TokenStream(new CppLexer(new StringReader("int k;")))
    assertTrue(stream.require(KW_INT))
    assertEquals(id("k"), stream.next())
  }

  @Test
  def testRequiredFailure() = {
    val stream = new TokenStream(new CppLexer(new StringReader("int k;")))
    assertFalse(stream.require(KW_FLOAT))
    assertEquals(kw(KW_INT), stream.next())
  }

  @Test
  def testRequireListOfTokens() = {
    val stream = new TokenStream(new CppLexer(new StringReader("int k;")))
    assertTrue(stream.require(Seq(KW_INT, KW_FLOAT)))
    assertEquals(id("k"), stream.next())
  }

  @Test
  def testRequiredFailedForListOfTokens() = {
    val stream = new TokenStream(new CppLexer(new StringReader("int k;")))
    assertFalse(stream.require(Seq(KW_DOUBLE, KW_FLOAT)))
    assertEquals(kw(KW_INT), stream.next())
  }
}
