package org.dubikdev.cpp

import org.junit.Test
import org.dubikdev.cpp.abi.Alignment
import org.dubikdev.cpp.semantic.{ETypeModifier, CompoundType, EFundamentalType, FundamentalType}
import org.dubikdev.cpp.semantic.EFundamentalType._
import org.junit.Assert._
import ETypeModifier._

class AlignmentTest {
  private def testFundamentalAlign(fundamentalType: EFundamentalType, expectedAlignment: Int) =
    assertEquals(expectedAlignment, Alignment.typeAlignment(new FundamentalType(fundamentalType)))

  @Test
  def testFundamentalTypesAlign() = {
    testFundamentalAlign(FT_INT, 4)
    testFundamentalAlign(FT_SIGNED_CHAR, 1)
    testFundamentalAlign(FT_SHORT_INT, 2)
    testFundamentalAlign(FT_LONG_INT, 8)
    testFundamentalAlign(FT_LONG_LONG_INT, 8)
  }

  private def pointerToAlignment(fundamentalType: EFundamentalType) =
    Alignment.typeAlignment(new CompoundType(TM_Pointer, new FundamentalType(fundamentalType)))

  private def lrefToAlignment(fundamentalType: EFundamentalType) =
    Alignment.typeAlignment(new CompoundType(TM_LValueRef, new FundamentalType(fundamentalType)))

  private def rrefToAlignment(fundamentalType: EFundamentalType) =
    Alignment.typeAlignment(new CompoundType(TM_RValueRef, new FundamentalType(fundamentalType)))


  @Test
  def testPointerTypes() = {
    assertEquals(8, pointerToAlignment(FT_INT))
    assertEquals(8, pointerToAlignment(FT_CHAR))
    assertEquals(8, lrefToAlignment(FT_INT))
    assertEquals(8, lrefToAlignment(FT_CHAR))
    assertEquals(8, rrefToAlignment(FT_INT))
    assertEquals(8, rrefToAlignment(FT_CHAR))
  }
}
