package org.dubikdev.cpp

import org.dubikdev.cpp.lexer.ETokenType._
import org.dubikdev.cpp.lexer.Token._
import org.junit.Assert._
import org.junit.Test

class LexerTest {
  @Test
  def testTokenizeIntk() ={
    val actual = CompilerFactory.tokenize("int k;")
    val expected = (kw(KW_INT), id("k"), kw(OP_SEMICOLON)).productIterator.toList

    assertEquals(actual, expected)
  }
}
