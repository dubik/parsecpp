package org.dubikdev.cpp

import org.junit.Test
import org.junit.Assert._

class DeclaratorInitializerTest extends NSInitBaseTest {
  @Test
  def intVariableInitializedWithOne() = {
    assertEquals("50 41 38 00 01 00 00 00", dumpToHex("int k = 1;"))
  }

  @Test
  def showErrorWhenConstVarIsNotInitialized() = {
    expectException {
      parse_simple_declaration("const int k; int main() {}")
    }
  }
}
