package org.dubikdev.cpp

import org.dubikdev.cpp.parser.DeclarationParser
import org.dubikdev.cpp.lexer.CppLexer
import java.io.StringReader
import org.dubikdev.cpp.semantic._
import org.junit.Assert._
import EFundamentalType._
import ETypeModifier._
import org.junit.Test
import org.dubikdev.cpp.EntryFactory._
import org.dubikdev.cpp.semantic.FunctionType
import org.dubikdev.cpp.semantic.CompoundType
import org.dubikdev.cpp.semantic.FundamentalType
import org.dubikdev.cpp.semantic.FunctionType
import org.dubikdev.cpp.semantic.CompoundType
import org.dubikdev.cpp.semantic.FundamentalType

class TypeParsingTest extends ParserBaseTest{
  def parse_and_type_to_string(source: String, declSpecifier: DeclSpecifier): String = {
    val ast = parse_simple_declaration(source).declaratorAst
    val t = ast.createType(declSpecifier)
    t.toString
  }

  def parse_and_type_to_string(source: String): String = {
    val declSpecifier = new DeclSpecifier()
    declSpecifier.setTypeSpecifier(new FundamentalType(EFundamentalType.FT_INT))
    parse_and_type_to_string(source, declSpecifier)
  }

  @Test
  def testSimpleIntK() = {
    assertEquals("int", parse_and_type_to_string("int k;"))
  }

  @Test
  def testSimplePointerToIntK() = {
    assertEquals("pointer to int", parse_and_type_to_string("int *k;"))
  }

  @Test
  def testSimplePointerToPointerIntK() = {
    assertEquals("pointer to pointer to int", parse_and_type_to_string("int **k;"))
  }

  @Test
  def testComplexFuncRetInt() = {
    assertEquals("function of (float, pointer to double) returning int", parse_and_type_to_string("int k(float, double *);"))
  }

  @Test
  def testSimplePointerToFunc() = {
    assertEquals("pointer to function of () returning int", parse_and_type_to_string("int (*k)();"))
  }

  @Test
  def testComplexFunc() = {
    assertEquals("function of (pointer to double) returning pointer to function of () returning int", parse_and_type_to_string("int (*k(double *))();"))
  }

  @Test
  def testVeryComplexFunc() = {
    assertEquals("function of (pointer to double) returning pointer to function of (float) returning function of (char) returning function of (int, pointer to pointer to int) returning int",
      parse_and_type_to_string("int (*k(double *))(float)(char)(int, int**);"))
  }

  @Test
  def testSimple5ArrayOfInt() = {
    assertEquals("array of 5 int", parse_and_type_to_string("int k[5];"))
  }

  @Test
  def parseFuncWithVoidParam() = {
    assertEquals("function of () returning int", parse_and_type_to_string("int f(void);"))
  }

  @Test
  def parseVaarg() = {
    assertEquals("function of (...) returning int", parse_and_type_to_string("int f(...);"))
  }

  @Test
  def parseSecondVaarg() = {
    assertEquals("function of (float, ...) returning int", parse_and_type_to_string("int f(float, ...);"))
  }

  @Test
  def parseSecondVaargWithoutCommaInBetween() = {
    assertEquals("function of (float, ...) returning int", parse_and_type_to_string("int f(float...);"))
  }

  @Test
  def parseFuncAsParam() = {
    assertEquals("function of (pointer to function of (int) returning void) returning int", parse_and_type_to_string("int g(void x(int));"))
  }

  @Test
  def parseFuncAsParamAndAnotherParam() = {
    assertEquals("function of (pointer to function of (int) returning void, pointer to int) returning int", parse_and_type_to_string("void g(void x(int), int* y);"))
  }

  @Test
  def testEqualFundamentalTypes() = {
    val bool = new FundamentalType(FT_BOOL)
    val int = new FundamentalType(FT_INT)
    val char16_t = new FundamentalType(FT_CHAR16_T)
    val bool1 = new FundamentalType(FT_BOOL)

    assertTrue(bool == bool1)
    assertFalse(bool == int)
    assertFalse(bool == char16_t)
    assertTrue(char16_t != bool1)
  }

  @Test
  def testEqualCombpoundTypes() = {
    val pointerBool = new CompoundType(TM_Pointer, new FundamentalType(FT_BOOL))
    val pointerBool1 = new CompoundType(TM_Pointer, new FundamentalType(FT_BOOL))
    val rvalueRefBool = new CompoundType(TM_RValueRef, new FundamentalType(FT_BOOL))
    val pointerInt = new CompoundType(TM_Pointer, new FundamentalType(FT_INT))

    assertTrue(pointerBool == pointerBool1)
    assertFalse(pointerBool == rvalueRefBool)
    assertFalse(pointerBool == pointerInt)
    assertFalse(pointerBool != pointerBool1)
  }

  @Test
  def testEqualComplexCombpoundTypes() = {
    val pointerBool = new CompoundType(TM_Pointer, new CompoundType(TM_Pointer, new FundamentalType(FT_BOOL)))
    val pointerBool1 = new CompoundType(TM_Pointer, new CompoundType(TM_Pointer, new FundamentalType(FT_BOOL)))

    assertTrue(pointerBool == pointerBool1)
    assertFalse(pointerBool != pointerBool1)
  }

  @Test
  def testEqualFunctionType() = {
    val funcA = new FunctionType(new FundamentalType(FT_BOOL))
    funcA.addParam(new FundamentalType(FT_INT))

    val funcB = new FunctionType(new FundamentalType(FT_BOOL))
    funcB.addParam(new FundamentalType(FT_INT))

    assertTrue(funcA == funcB)
    assertFalse(funcA != funcB)
  }

  @Test
  def pointerToConstInt() {
    val expected = List(startNS(), variable("i pointer to const int"), endNS())
    val actual = parse_and_walk("const int * i;")

    assertEquals(toStr(expected), toStr(actual))
  }
}
