package org.dubikdev.cpp

import org.dubikdev.cpp.EntryFactory._
import org.dubikdev.cpp.semantic.{FundamentalType, VariableEntity, DeclarationParsingContext}
import org.junit.Assert._
import org.dubikdev.cpp.semantic.EFundamentalType._
import org.junit.Test


class DeclContextTest {
  private def doTest(func: (DeclarationParsingContext) => Unit, expected: List[Entry]) = {
    val ctx = new DeclarationParsingContext
    val visitor = new TestVisitor
    func(ctx)
    ctx.defaultNamespace.accept(visitor)

    assertEquals(expected, visitor.entries)
  }

  @Test
  def testAddOneNamespace() = {
    val ctx = new DeclarationParsingContext
    val visitor = new TestVisitor
    ctx.defaultNamespace.accept(visitor)
    val expected = List(startNS(), endNS())
    assertEquals(expected, visitor.entries)
  }

  @Test
  def testAddOneInlineNamespace() = {
    def test(ctx: DeclarationParsingContext) = {
      ctx.pushNamespace("A", inline = false)
      ctx.commitNamespace()
    }

    doTest(test, List(startNS(), startNS("A"), endNS(), endNS()))
  }

  @Test
  def testHandleReopeningNamespaces() = {
    def test(ctx: DeclarationParsingContext) = {
      ctx.pushNamespace("A", inline = false)
      ctx.commitNamespace()
      ctx.pushNamespace("A", inline = false)
      ctx.pushNamespace("B", inline = false)
      ctx.commitNamespace()
      ctx.commitNamespace()
    }

    doTest(test, List(startNS(), startNS("A"), startNS("B"), endNS(), endNS(), endNS()))
  }

  @Test
  def testUsingVariable(): Unit = {
    val ctx = new DeclarationParsingContext
    ctx.pushNamespace("A", inline = false)
    val variable = new VariableEntity("i", new FundamentalType(FT_INT))
    ctx.addEntity(variable)
    ctx.commitNamespace()

    ctx.using(List("A"), "i")

    val entity = ctx.lookup(List[String](), "i")
    assertTrue(entity.isDefined)
    assertEquals(variable, entity.get)
  }

  /*
    namespace A {}
    int A;
  */
  @Test(expected = classOf[Exception])
  def namespaceAndVarDeclWithTheSameNameShouldFail(): Unit = {
    val ctx = new DeclarationParsingContext
    ctx.pushNamespace("A", inline = false)
    ctx.commitNamespace()
    ctx.addEntity(new VariableEntity("A", new FundamentalType(FT_INT)))
  }
}
