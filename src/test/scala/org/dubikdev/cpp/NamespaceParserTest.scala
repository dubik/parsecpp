package org.dubikdev.cpp

import org.dubikdev.cpp.EntryFactory._
import org.junit.Assert._
import org.junit.{After, Before, Test}
import org.dubikdev.cpp.support.PA

class NamespaceParserTest extends ParserBaseTest {
  @Before
  def setUp() = {
    PA.isPA7 = true
  }

  @After
  def tearDown() = {
    PA.isPA7 = false
  }

  @Test
  def testUsingNamespace() = {
    val expected = List(startNS(), variable("x char"), startNS("A"), typedef("T int"), endNS(), startNS("B"), typedef("T char"), endNS(), endNS())
    val source =
      """
        |namespace A { typedef int T; }
        |namespace B { typedef char T; }
        |using namespace B;
        |T x;
      """.stripMargin
    val actual = parse_and_walk(source)

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def testNamespaceAlias() = {
    val expected = List(startNS(), variable("x char"),
      startNS("A"), typedef("T int"), endNS(),
      startNS("B"), typedef("T char"), endNS(),
      startNS("C"), typedef("T double"), endNS(),
      endNS())
    val source =
      """
        |namespace A { typedef int T; }
        |namespace B { typedef char T; }
        |namespace C { typedef double T; }
        |namespace D = B;
        |D::T x;
      """.stripMargin
    val actual = parse_and_walk(source)

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def testOuterInnerTypedef() = {
    val expected = List(startNS(),
      startNS("Outer"), startNS("Inner"),

      variable("i int"), variable("j double"),

      typedef("I double"), endNS(),
      typedef("I int"), endNS(),
      endNS())
    val source =
      """
        |namespace Outer {
        |	typedef int I;
        |	namespace Inner {
        |		I i;
        |		typedef double I;
        |		I j;
        |	}
        |}
      """.stripMargin
    val actual = parse_and_walk(source)

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def testSimpleUnnamedNamespace() = {
    val expected = List(startNS(),
      variable("k int"),
      startNS("__unique__1979unique_not_needed"),
      typedef("I int"), endNS(),
      endNS())

    val source =
      """
        |namespace {
        |	typedef int I;
        |}
        |I k;
      """.stripMargin
    val actual = parse_and_walk(source)

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def testExternVar() = {
    val expected = List(startNS(),
      startNS("Q"), startNS("V"),
      variable("i int"),
      endNS(), endNS(),
      endNS())

    val source =
      """
        |namespace Q
        |{
        |	namespace V
        |	{
        |		extern int i;
        |	}
        |	int V::i;
        |}
      """.stripMargin
    val actual = parse_and_walk(source)

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def testSimpleUsingTypedef() = {
    val expected = List(startNS(),
      variable("k int"),
      startNS("Q"),
      typedef("T int"),
      endNS(),
      endNS())

    val source =
      """
        |namespace Q {
        |  typedef int T;
        |}
        |
        |using Q::T;
        |
        |T k;""".stripMargin
    val actual = parse_and_walk(source)

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def testSimpleUsingAlias() = {
    val expected = List(startNS(), variable("i int"), endNS())

    val source =
      """
        |using T = int;
        |T i;
      """.stripMargin
    val actual = parse_and_walk(source)

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def testLookupInNamedInlinedNamespace() = {
    val expected = List(startNS(), variable("i int"), startNS("A"), inlineNS(), typedef("T int"), endNS(), endNS())

    val source =
      """
        |inline namespace A{
        |  typedef int T;
        |}
        |T i;
      """.stripMargin
    val actual = parse_and_walk(source)

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def testTwoUnnamedNamespaces() = {
    val expected = List(startNS(), startNS("__unique__1979unique_not_needed"), endNS(), startNS("A"), inlineNS(),
      startNS("__unique__1979unique_not_needed"), inlineNS(), endNS(), endNS(), endNS())

    // The catch here is that inlined unnamed namespace of A is available also in global namespace
    // so when we add second unnamed namespace to global namespace, lookup will show 2 unnamed namespaces
    // available.
    val source =
      """
        |namespace {
        |}
        |
        |inline namespace A {
        |  inline namespace {
        |  }
        |}
        |
        |namespace {
        |}
      """.stripMargin
    val actual = parse_and_walk(source)

    assertEquals(toStr(expected), toStr(actual))
  }

  @Test
  def openingNamespaceWhichWasUsedAsAnAlias() = {
    val source =
      """
        |namespace A {
        |}
        |namespace B = A;
        |namespace B {
        |}
      """.stripMargin

    expectException {
      parse_and_walk(source)
    }
  }

  @Test
  def namespaceAliasToSelf() = {
    val source =
      """
        |namespace X { }
        |namespace X = X;
      """.stripMargin

    expectException {
      parse_and_walk(source)
    }
  }

  @Test
  def functionRedefinedWithNamespace() = {
    val source =
      """
        |void x(...) {}
        |namespace x {}
      """.stripMargin

    expectException {
      parse_and_walk(source)
    }
  }

  @Test
  def redefiningAliasDeclaration() = {
    val source =
      """
        |using x = int;
        |namespace x {}
      """.stripMargin

    expectException {
      parse_and_walk(source)
    }
  }
}
