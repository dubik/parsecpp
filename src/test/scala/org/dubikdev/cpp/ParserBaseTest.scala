package org.dubikdev.cpp

import org.dubikdev.cpp.parser.DeclarationParser
import org.dubikdev.cpp.lexer.CppLexer
import java.io.StringReader
import org.junit.Assert._

trait ParserBaseTest {
  def parse_simple_declaration(source: String): DeclarationParser = {
    val stream = new TokenStream(new CppLexer(new StringReader(source)))
    val parser = new DeclarationParser(stream, "unique_not_needed")
    assert(parser.translation_unit())
    parser
  }

  def parse_and_walk(source: String): List[Entry] = {
    val visitor = new TestVisitor
    val context = parse_simple_declaration(source).context
    context.defaultNamespace.accept(visitor)
    visitor.entries.toList
  }

  def toStr(list: List[Entry]): String =
    list.map(_.toString).mkString("\n")

  def expectException(func: => Unit): Unit = {
    try {
      func
      fail("Exception expected and wasn't thrown")
    } catch {
      case e: Exception =>
    }
  }
}
