package org.dubikdev.cpp

import org.dubikdev.cpp.semantic.EEntityType

class Entry(val name: String, val entityType: EEntityType) {
  override def toString: String = s"[$entityType: $name]"

  override def equals(o: scala.Any): Boolean = {
    if (o == null || !o.isInstanceOf[Entry])
      return false

    val e = o.asInstanceOf[Entry]
    if (name.equals(e.name) && entityType == e.entityType)
      return true

    return false
  }
}

object EntryFactory {
  def startNS() = new Entry("start unnamed", EEntityType.ET_Namespace)

  def startNS(name: String) = new Entry(s"start $name", EEntityType.ET_Namespace)

  def endNS() = new Entry("end namespace", EEntityType.ET_Namespace)

  def inlineNS() = new Entry("inline namespace", EEntityType.ET_Namespace)

  def variable(str: String) = new Entry(s"var $str", EEntityType.ET_Variable)

  def func(str: String) = new Entry(s"function $str", EEntityType.ET_Function)

  def typedef(str: String) = new Entry(s"typedef $str", EEntityType.ET_Typedef)
}
