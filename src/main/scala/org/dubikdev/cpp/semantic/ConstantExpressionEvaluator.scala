package org.dubikdev.cpp.semantic

import org.dubikdev.cpp.ast.{IdExpression, LiteralExpression, Expression}

object ConstantExpressionEvaluator {
  def calculate(expr: Expression): String = {
    expr match {
      case LiteralExpression(literal) => literal
      case IdExpression(qName, entity) => calculateEntity(entity)
      case _ => throw new SemanticError(s"Can't process $expr")
    }
  }

  private def calculateEntity(entity: NamedEntity): String = {
    entity match {
      case v: VariableEntity =>
        v.initializer match {
          case i: CopyInitializer => calculate(i.expression)
          case n: NoInitializer => throw new SemanticError(s"There is no initializer!")
          case _ => throw new Exception("Check me out")
        }
      case _ => throw new Exception(s"Can't calculate $entity")
    }
  }
}
