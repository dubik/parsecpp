package org.dubikdev.cpp.semantic

import scala.collection.mutable.ListBuffer
import scala.collection.mutable
import EEntityType._

object QualifiedName {
  def apply(name: String, nestedNameSpecifier: List[String]) = new QualifiedName(name, nestedNameSpecifier)

  def apply(qName: List[String]): QualifiedName =
    qName match {
      case Nil => throw new Exception("This is barely useful to create QualifiedName with empty qName")
      case x :: Nil => new QualifiedName(x, Nil)
      case x :: xs =>
        val reversedQName = qName.reverse
        val name = reversedQName.head
        val nestedNameSpecifier = reversedQName.tail.reverse
        new QualifiedName(name, nestedNameSpecifier)
    }
}

class QualifiedName(val name: String, val nestedNameSpecifier: List[String]) {
  override def equals(obj: scala.Any): Boolean = obj match {
    case that: QualifiedName => name == that.name && nestedNameSpecifier == that.nestedNameSpecifier
    case _ => false
  }

  override def toString: String = (nestedNameSpecifier :+ name) mkString "::"
}

trait EntityVisitor {
  def visit(entity: VariableEntity)

  def visit(entity: FunctionEntity)

  def visit(entity: TypedefEntity)

  def visitEnter(entity: NamespaceEntity)

  def visitExit(entity: NamespaceEntity)

  def isPA7: Boolean = true
}

trait Entity {
  def accept(visitor: EntityVisitor)

  def entityType(): EEntityType
}

abstract class NamedEntity(val name: String) extends Entity {
}

object Specifiers {
  def EmptySpecifier = new Specifiers(mutable.Set())

  def ExternSpecifier = new Specifiers(mutable.Set(EDeclSpecifier.DS_Extern))
}

class Specifiers(val specifiers: mutable.Set[EDeclSpecifier]) {
  def isExtern: Boolean = specifiers.contains(EDeclSpecifier.DS_Extern)

  def isTypedef: Boolean = specifiers.contains(EDeclSpecifier.DS_TypeDef)

  def isThreadLocal: Boolean = specifiers.contains(EDeclSpecifier.DS_ThreadLocal)

  def isStatic: Boolean = specifiers.contains(EDeclSpecifier.DS_Static)

  def isInline: Boolean = specifiers.contains(EDeclSpecifier.DS_Inline)

  def isConstExpr: Boolean = specifiers.contains(EDeclSpecifier.DS_ConstExpr)
}

class VariableEntity(name: String, val nestedNameSpecifier: List[String],
                     val varType: Type, val specifiers: Specifiers,
                     val initializer: Initializer) extends NamedEntity(name) {
  def this(name: String, varType: Type) = this(name, List(), varType, new Specifiers(mutable.Set()), new NoInitializer)

  def this(name: String, varType: Type, initializer: Initializer) = this(name, List(), varType, Specifiers.EmptySpecifier, initializer)

  override def accept(visitor: EntityVisitor): Unit = visitor.visit(this)

  override def entityType(): EEntityType = EEntityType.ET_Variable

  override def toString: String = {
    val qualifiedName = nestedNameSpecifier :+ name
    s"[${entityType()}] $varType ${qualifiedName.mkString("::")}"
  }
}

class TypedefEntity(name: String, val typedefType: Type) extends NamedEntity(name) {
  override def accept(visitor: EntityVisitor): Unit = visitor.visit(this)

  override def entityType(): EEntityType = EEntityType.ET_Typedef

  override def toString: String = s"[${entityType()}] $name"
}

class AliasEntity(name: String, typedefType: Type) extends TypedefEntity(name, typedefType) {
  override def entityType(): EEntityType = EEntityType.ET_Typedef
}

class FunctionEntity(name: String, val funcType: Type) extends NamedEntity(name) {
  override def accept(visitor: EntityVisitor): Unit = visitor.visit(this)

  override def entityType(): EEntityType = EEntityType.ET_Function

  override def toString: String = s"[${entityType()}] $name $funcType"

  override def equals(obj: Any): Boolean =
    obj match {
      case that: FunctionEntity => name == that.name && entityType() == that.entityType() && funcType == that.funcType
      case _ => false
    }
}

object NamespaceEntityConstants {
  val UniqueNamespaceName = "__unique__1979"
}

class NamespaceEntity(name: String, val inline: Boolean) extends NamedEntity(name) {
  private val entries = ListBuffer[NamedEntity]()
  private val entities = new mutable.HashMap[String, NamedEntity]

  val attachedNamespaces = mutable.Set[NamespaceEntity]()
  val namespaceAliases = mutable.HashMap[String, NamespaceEntity]()
  val usingEntities = mutable.HashMap[String, NamedEntity]()
  val aliases = mutable.HashMap[String, AliasEntity]()

  def this() = this("unnamed", false)

  def this(n: String) = this(n, false)

  override def toString: String =
    s"$name: ${entries.size} item(s)"


  override def equals(obj: Any): Boolean =
    obj match {
      case that: NamespaceEntity => name == that.name && inline == that.inline &&
        entries == that.entries && entities == that.entities && namespaceAliases == that.namespaceAliases
      case _ => false
    }

  override def accept(visitor: EntityVisitor): Unit = {
    visitor.visitEnter(this)

    if (visitor.isPA7) {
      def eachEntry(entityType: EEntityType) = entries.filter(_.entityType() == entityType).foreach(_.accept(visitor))
      List(ET_Variable, ET_Function, ET_Namespace, ET_Typedef).foreach(eachEntry(_))
    } else {
      entries.filter((item) => List(ET_Variable, ET_Function, ET_Namespace).contains(item.entityType())).foreach(_.accept(visitor))
    }

    visitor.visitExit(this)
  }

  override def entityType(): EEntityType = EEntityType.ET_Namespace

  def isUnnamed: Boolean = name.startsWith(NamespaceEntityConstants.UniqueNamespaceName)

  def add(entity: NamedEntity): Unit = {
    if (entity.entityType() == ET_Namespace) {
      shallowLookup(entity.name) match {
        case Some(ns) =>
          if (ns.entityType() == ET_Namespace) {
            entity.asInstanceOf[NamespaceEntity].mergeTo(ns.asInstanceOf[NamespaceEntity])
            return
          }

        case None =>
      }
    }

    entries += entity
    entities.put(entity.name, entity)
  }

  def replaceVariable(targetEntity: VariableEntity, newEntity: VariableEntity) = {
    val index = entries.indexOf(targetEntity)
    entries.update(index, newEntity)

    entities.put(targetEntity.name, newEntity)
  }

  def mergeTo(namespace: NamespaceEntity): Unit = {
    entries.foreach(namespace.add(_))
  }

  def exists(name: String): Boolean =
    entities.get(name).isDefined

  def shallowLookup(name: String): Option[NamedEntity] =
    entities.get(name)

  def lookup(name: String): Option[NamedEntity] =
    lookup(name, ET_Function, ET_Namespace, ET_Typedef, ET_Variable, ET_AliasEntity)

  def lookup(name: String, filters: EEntityType*): Option[NamedEntity] = {
    val entity = shallowLookup(name) match {
      case Some(x) => {
        if (filters.contains(x.entityType()))
          Some(x)
        else
          None
      }

      case None => None
    }

    entity match {
      case Some(x) => Some(x)
      case None => {
        val entities = deepLookup(name).filter(item => filters.contains(item.entityType()))
        if (entities.size > 1)
          throw new Exception(s"'$name' : Ambiguous symbol")
        else if (entities.size == 1)
          Some(entities.head)
        else
          None
      }
    }
  }

  private def deepLookup(name: String): List[NamedEntity] = {
    var res = List[NamedEntity]()

    lookupInNamespaceAliases(name) match {
      case Some(ns) => res ::= ns
      case None =>
    }

    res :::= lookupInAttachedNamespaces(name)

    lookupInUsingEntities(name) match {
      case Some(entity) => res ::= entity
      case None =>
    }

    lookupInAliases(name) match {
      case Some(entity) => res ::= entity
      case None =>
    }

    res :::= lookupInInlinedNamespaces(name)

    res
  }

  private def lookupInNamespaces(list: Traversable[NamespaceEntity], name: String): List[NamedEntity] =
    list.foldLeft(List[NamedEntity]())((acc, ns) =>
      ns.lookup(name) match {
        case Some(x) => x :: acc
        case None => acc
      }
    )

  private def findInlinedNamespaces() =
    entries.foldLeft(List[NamespaceEntity]())((acc, ns) => {
      ns match {
        case x: NamespaceEntity => if (x.inline && !x.isUnnamed) x :: acc else acc
        case _ => acc
      }
    })

  private def lookupInInlinedNamespaces(name: String): List[NamedEntity] =
    lookupInNamespaces(findInlinedNamespaces(), name)

  private def lookupInAttachedNamespaces(name: String): List[NamedEntity] =
    lookupInNamespaces(attachedNamespaces, name)

  private def lookupInNamespaceAliases(name: String): Option[NamespaceEntity] =
    namespaceAliases.get(name)

  private def lookupInUsingEntities(name: String): Option[NamedEntity] =
    usingEntities.get(name)

  private def lookupInAliases(name: String): Option[NamedEntity] =
    aliases.get(name)
}