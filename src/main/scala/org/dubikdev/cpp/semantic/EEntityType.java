package org.dubikdev.cpp.semantic;

public enum EEntityType {
    ET_Object,
    ET_Namespace,
    ET_Variable,
    ET_Function,
    ET_Typedef,
    ET_AliasEntity
}
