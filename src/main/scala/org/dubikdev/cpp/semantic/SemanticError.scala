package org.dubikdev.cpp.semantic

class SemanticError(str: String) extends Exception {
  override def getMessage: String = str
}
