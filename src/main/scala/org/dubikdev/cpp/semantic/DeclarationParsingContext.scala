package org.dubikdev.cpp.semantic

import scala.collection.mutable
import org.dubikdev.cpp.lexer.{ETokenType, Token}
import Token._
import ETokenType._
import org.dubikdev.cpp.utils.CollectionsHelper._
import EEntityType._
import ETypeModifier._
import org.dubikdev.cpp.ast.{IdExpression, Expression, DeclAst}
import EDeclSpecifier._
import EFundamentalType._
import org.dubikdev.cpp.support.PA

class DeclSpecifier {
  private var isConst = false
  private var isVolatile = false
  private var extern = false
  private var typeSpecifier: Type = null
  private var initializer: Initializer = null

  val dsSpecifiers = mutable.Set[EDeclSpecifier]()

  def setDeclSpecifier(specifier: EDeclSpecifier) = {
    if (dsSpecifiers.contains(specifier))
      throw new IllegalArgumentException(s"duplicate specifier ${specifier.toString}")

    dsSpecifiers += specifier
  }

  def existsSpecifier(specifier: EDeclSpecifier): Boolean = dsSpecifiers.contains(specifier)

  def setInitializer(init: Initializer) = initializer = init

  def getInitializer = initializer

  def isTypedef = existsSpecifier(DS_TypeDef)

  def setExtern() =
    if (extern)
      throw new IllegalArgumentException("duplicate specifier")
    else
      extern = true

  def isExtern = extern

  def isTypeSpecifierDefined = typeSpecifier != null

  def setCVQualifier(token: Token) =
    if (token == kw(KW_CONST) && !isConst) {
      isConst = true
    } else if (token == kw(KW_VOLATILE) && !isVolatile) {
      isVolatile = true
    } else {
      throw new IllegalArgumentException("duplicate specifier")
    }

  def setTypeSpecifier(specifier: Type) = {
    if (typeSpecifier != null)
      throw new IllegalArgumentException("duplicate specifier")

    typeSpecifier = specifier
  }

  def getTypeSpecifier = typeSpecifier

  def createBaseType(): Type = {
    typeSpecifier match {
      case compoundType: CompoundType =>
        // 3.9.3.5 - Cv-qualiﬁers applied to an array type attach to the underlying element type, so the notation “cv T,” where
        // T is an array type, refers to an array whose elements are so-qualiﬁed.
        if (compoundType.modifier == TM_Array || compoundType.modifier == TM_ArrayOfUnknownBound) {
          new CompoundType(compoundType.modifier, compoundType.bound, wrapTypeWithCVQualifiers(compoundType.subType))
        } else {
          wrapTypeWithCVQualifiers(typeSpecifier)
        }
      case _ => wrapTypeWithCVQualifiers(typeSpecifier)
    }
  }

  private def wrapTypeWithCVQualifiers(t: Type) = {
    var newType = t
    if (isConst)
      newType = new CompoundType(ETypeModifier.TM_Const, newType)

    if (isVolatile)
      newType = new CompoundType(ETypeModifier.TM_Volatile, newType)

    newType
  }
}

trait ITypeResolver {
  def resolveType(src: String): Option[Type]

  def resolveType(name: String, qualifiedSpecifier: List[String]): Option[Type]
}

class DeclarationParsingContext(val unique: String) extends ITypeResolver {
  def this() = this("not_so_unique")

  val defaultNamespace = new NamespaceEntity()

  private val namespaces = mutable.Stack(defaultNamespace)

  def pushNamespace(name: String, inline: Boolean): NamespaceEntity = {
    makeSureNotExtendingInlineNamespace(name, inline)
    makeSureNotUsingNameFromNamespaceAliases(name)
    makeSureNotRedefiningSymbol(name, ET_Namespace)

    val namespaceEntity = new NamespaceEntity(name, inline)
    namespaces.push(namespaceEntity)
    namespaceEntity
  }

  private def makeSureNotRedefiningSymbol(name: String, entityType: EEntityType) = {
    if (namespaces.top.aliases.contains(name))
      throw new SemanticError(s"redefinition of '$name' as different kind of symbol")

    namespaces.top.shallowLookup(name) match {
      case Some(namedEntity) if namedEntity.entityType() != entityType => throw new SemanticError(s"redefinition of '$name' as different kind of symbol")
      case _ =>
    }
  }

  private def makeSureNotExtendingInlineNamespace(nsName: String, inline: Boolean) = {
    // Inline namespace can't extend existing, non inlined namespace
    if (inline && !nsName.isEmpty) {
      namespaces.top.lookup(nsName, ET_Namespace) match {
        case Some(ns) =>
          if (!ns.asInstanceOf[NamespaceEntity].inline)
            throw new SemanticError("extension namespace cannot be inline")
        case None =>
      }
    }
  }

  private def makeSureNotUsingNameFromNamespaceAliases(nsName: String) = {
    if (namespaces.top.namespaceAliases.contains(nsName))
      throw new SemanticError(s"nsName already exists")
  }

  /**
   * Push unnamed namespace. It will create unique namespace and use it.
   * <pre>
   * namespace <i>unique</i> {
   * }
   * using namespace <i>unique</i>;
   * </pre>
   * @param inline if namespace should be inlined
   */
  def pushUnnamedNamespace(inline: Boolean): NamespaceEntity = {
    val namespaceName = createUniqueNamespaceName()
    val namespaceEntity: NamespaceEntity = new NamespaceEntity(namespaceName, inline)
    namespaces.push(namespaceEntity)
    namespaceEntity
  }

  private def createUniqueNamespaceName(): String = NamespaceEntityConstants.UniqueNamespaceName + unique

  def commitNamespace() = {
    val top = namespaces.pop()
    val parent = namespaces.top
    if (top.name == "__temp__") {
      top.mergeTo(parent)
    } else {
      parent.add(top)
    }
  }

  def discardNamespace() = namespaces.pop()

  def pushTempNamespace(): NamespaceEntity = pushNamespace("__temp__", inline = false)

  def addEntity(entity: NamedEntity): Unit = {
    namespaces.top.shallowLookup(entity.name) match {
      case Some(x) =>
        if (x != entity)
          throw new Exception(s"Redefinition of ${entity.name} as a different kind of symbol")
      case None => namespaces.top.add(entity)
    }
  }

  def replaceVariable(target: VariableEntity, newEntity: VariableEntity) = {
    var ns = namespaces.top
    if (newEntity.nestedNameSpecifier.nonEmpty)
      ns = lookupNamespace(newEntity.nestedNameSpecifier)

    ns.replaceVariable(target, newEntity)
  }

  private def lookupNamespace(qName: List[String]): NamespaceEntity = {
    val possibleMatch = qName match {
      case Nil => Some(namespaces.top)
      case x :: Nil => lookup(List(), x, ET_Namespace)
      case _ =>
        // Just getting last element of qName as a name and the rest should be qName
        val reversedQName = qName.reverse
        val nsName = reversedQName.head
        val nsPath = reversedQName.tail.reverse
        lookup(nsPath, nsName, ET_Namespace).asInstanceOf[NamespaceEntity]
    }

    possibleMatch match {
      case Some(ns) => ns.asInstanceOf[NamespaceEntity]
      case None => throw new SemanticError(s"Can't find namespace at: $qName")
    }
  }

  def addDeclaratorEntity(declaratorAst: DeclAst, declSpecifier: DeclSpecifier) = {
    val finalType = declaratorAst.createType(declSpecifier)
    checkTypeIsOk(finalType, declaratorAst.name())

    var entity: NamedEntity = null

    if (declSpecifier.isTypedef) {
      // Create typedef
      entity = new TypedefEntity(declaratorAst.name(), finalType)
      addEntity(entity)
    } else if (finalType.isFunctionType) {
      // Create function
      if (!PA.isPA7) {
        checkIfFunctionWasDefined(declaratorAst.name(), finalType)
      }
      addEntity(new FunctionEntity(declaratorAst.name(), finalType))
    } else {
      // Create variable
      createVariableEntityAndAppendToNamespace(declaratorAst, declSpecifier, finalType)
    }
  }

  private def checkTypeIsOk(t: Type, name: String): Unit = {
    t match {
      case CompoundType(TM_Pointer, _, CompoundType(TM_LValueRef, _, _)) =>
        throw new SemanticError(s"'$name' declared as a pointer to a reference")
      case CompoundType(TM_LValueRef, _, FundamentalType(FT_VOID)) =>
        throw new SemanticError(s"$name' declared as a reference to void")
      case CompoundType(TM_LValueRef, _, CompoundType(TM_LValueRef, _, _)) =>
        throw new SemanticError(s"'$name' declared as a reference to reference")
      case _ =>
    }
  }

  def makeAlias(declaratorAst: DeclAst, declSpecifier: DeclSpecifier): Unit = {
    val finalType = declaratorAst.createType(declSpecifier)
    namespaces.top.aliases.put(declaratorAst.name(), new AliasEntity(declaratorAst.name(), finalType))
  }

  private def checkIfFunctionWasDefined(name: String, finalType: Type): Unit = {
    lookup(List(), name, ET_Function) match {
      case Some(x) => throw new SemanticError(s"Function $name already defined")
      case _ =>
    }
  }

  private def createVariableEntityAndAppendToNamespace(declaratorAst: DeclAst, declSpecifier: DeclSpecifier, finalType: Type): Unit = {
    val qName = declaratorAst.nestedNameSpecifier()
    val name = declaratorAst.name()
    lookupVariable(qName, name) match {
      case Some(varEntity) => {
        // TODO simplify this.
        if (varEntity.varType != finalType) {
          val oneOrBothExtern = varEntity.specifiers.isExtern | declSpecifier.isExtern
          if (oneOrBothExtern) {
            if (equalArrayTypes(varEntity.varType, finalType)) {
              val moreSpecificType = getMoreSpecificArrayType(varEntity.varType, finalType)
              val newVarIsExtern: Boolean = varEntity.specifiers.isExtern & declSpecifier.isExtern
              // TODO copying specifiers should be handled with care
              val newSpecifers: Specifiers = new Specifiers(mutable.Set())
              if (newVarIsExtern)
                newSpecifers.specifiers += DS_Extern

              replaceVariable(varEntity, new VariableEntity(declaratorAst.name(), qName, moreSpecificType, newSpecifers, declSpecifier.getInitializer))
            } else {
              throw new Exception(s"Redefinition of $name with different type")
            }
          }
        } else {
          if (!PA.isPA7) {
            var entity: VariableEntity = null
            if (varEntity.specifiers.isExtern && !declSpecifier.isExtern) {
              val newSpecifers = new Specifiers(mutable.Set())
              entity = new VariableEntity(declaratorAst.name(), qName, finalType, new Specifiers(declSpecifier.dsSpecifiers), declSpecifier.getInitializer)
              replaceVariable(varEntity, entity)
            } else {
              entity = new VariableEntity(declaratorAst.name(), qName, finalType, new Specifiers(declSpecifier.dsSpecifiers), declSpecifier.getInitializer)
              Validators.validateVariable(entity)
              addEntity(entity)
            }
          }
        }
        // TODO handle extern definition&declaration
      }
      case None =>
        val entity = new VariableEntity(declaratorAst.name(), qName, finalType, new Specifiers(declSpecifier.dsSpecifiers), declSpecifier.getInitializer)
        if (!PA.isPA7)
          Validators.validateVariable(entity)
        addEntity(entity)
    }
  }

  private def getMoreSpecificArrayType(t1: Type, t2: Type): Type = {
    if (t1.isCompoundType && t2.isCompoundType) {
      val c1 = t1.asInstanceOf[CompoundType]
      val c2 = t2.asInstanceOf[CompoundType]
      if (c1.modifier == TM_ArrayOfUnknownBound)
        c2
      else
        c1
    } else {
      throw new AssertionError("t1 and t2 must be compound types, because they should be arrays")
    }
  }

  private def equalArrayTypes(t1: Type, t2: Type): Boolean = {
    if (t1.isCompoundType && t2.isCompoundType) {
      val c1 = t1.asInstanceOf[CompoundType]
      val c2 = t2.asInstanceOf[CompoundType]
      val modifiersSet = Set(TM_ArrayOfUnknownBound, TM_Array)
      if (modifiersSet.exists(_ == c1.modifier) && modifiersSet.exists(_ == c2.modifier)) {
        return c1.subType == c2.subType
      } else {
        c1 == c2
      }
    } else {
      t1 == t2
    }
  }

  def lookup(nestedNameSpecifier: List[String], name: String): Option[NamedEntity] = {
    lookup(nestedNameSpecifier, name, ET_Namespace, ET_Function, ET_Variable, ET_AliasEntity)
  }

  def lookup(nestedNameSpecifier: List[String], name: String, filters: EEntityType*): Option[NamedEntity] = {
    findRootNamespace(nestedNameSpecifier) match {
      case Some(rootNS) => rootNS.lookup(name, filters: _*)
      case None => None
    }
  }

  private def lookupVariable(qualifiedName: List[String], varName: String): Option[VariableEntity] = {
    findRootNamespace(qualifiedName) match {
      case Some(rootNS) => descentToEntity(rootNS, varName, qualifiedName.safeTail, ET_Variable) match {
        case Some(varEntity) => Some(varEntity.asInstanceOf[VariableEntity])
        case None => None
      }
      case None => None
    }
  }

  def using(nestedNameSpecifier: List[String], name: String) = {
    lookup(nestedNameSpecifier, name, ET_Function, ET_Variable, ET_Typedef) match {
      case Some(x) => addUsingEntity(x)
      case _ => throw new Exception(s"$name is not a member of ${nestedNameSpecifier.mkString("::")}")
    }
  }

  private def addUsingEntity(entity: NamedEntity): Unit = {
    if (!namespaces.top.usingEntities.contains(entity.name)) {
      namespaces.top.usingEntities.put(entity.name, entity)
    }
  }

  def makeNamespaceAlias(name: String, qualifiedNS: List[String]) = {
    makeSureAliasIsNotToSelf(name, qualifiedNS)

    findRootNamespace(qualifiedNS) match {
      case Some(rootNS) => {
        val aliases = namespaces.top.namespaceAliases
        if (!aliases.contains(name)) {
          aliases.put(name, rootNS)
        } else if (!aliases(name).equals(rootNS)) {
          throw new Exception("Redefinition of alias is prohibited")
        }
      }

      case None => throw new Exception("qualified name specifier is not valid")
    }
  }

  /*
    Since we can have alias to itself we should check that we do not create
    alias with the name already taken by namespace.
   */
  private def makeSureAliasIsNotToSelf(name: String, qName: List[String]): Unit = {
    lookup(List(), name, ET_Namespace) match {
      case Some(ns) if !namespaces.top.namespaceAliases.contains(name) => throw new SemanticError(s"$name is an original-namespace-name")
      case _ =>
    }
  }

  def usingNamespace(fullyQualifiedName: List[String]) = {
    findRootNamespace(fullyQualifiedName) match {
      case Some(rootNS) => descentToNamespace(rootNS, fullyQualifiedName.tail) match {
        case Some(targetNS) => namespaces.top.attachedNamespaces += targetNS
        case None =>
      }
      case None =>
    }
  }

  def isFundamentalType(src: String): Boolean = FundamentalTypes.str2TypeMap.contains(src)

  override def resolveType(src: String): Option[Type] = {
    if (isFundamentalType(src))
      Some(new FundamentalType(FundamentalTypes.str2TypeMap(src)))
    else {
      lookupInCurrentAndUp(src, EEntityType.ET_Typedef) match {
        case Some(x) => Some(x.asInstanceOf[TypedefEntity].typedefType)
        case None => None
      }
    }
  }

  /**
   * Searches for the <code>EEntityType</code> in the current namespace, then in the parent
   * and so on
   * @param name name of entity to lookup
   * @param filter type of entity
   * @return named entity or <code>None</code>
   */
  private def lookupInCurrentAndUp(name: String, filter: EEntityType): Option[NamedEntity] =
    namespaces.existsEx(ns => ns.lookup(name, filter))

  private def lookupNamespaceInCurrentAndUp(name: String): Option[NamespaceEntity] =
    lookupInCurrentAndUp(name, EEntityType.ET_Namespace) match {
      case Some(ns) => Some(ns.asInstanceOf[NamespaceEntity])
      case None => None
    }

  /**
   * Finds entity from specified namespace with specified name of specified type
   * @param rootNS namespace from where search will start
   * @param name name of item to find
   * @param path list of namespaces
   * @param filter type of the entity
   * @return entity
   */
  private def descentToEntity(rootNS: NamespaceEntity, name: String, path: List[String], filter: EEntityType): Option[NamedEntity] = {
    if (path.isEmpty)
      rootNS.lookup(name, filter)
    else {
      rootNS.lookup(name, EEntityType.ET_Namespace) match {
        case Some(nextNS) => descentToEntity(nextNS.asInstanceOf[NamespaceEntity], name, path.tail, filter)
        case None => None
      }
    }
  }

  private def descentToNamespace(rootNS: NamespaceEntity, path: List[String]): Option[NamespaceEntity] = {
    path match {
      case Nil => Some(rootNS)
      case x :: xs => rootNS.lookup(x, EEntityType.ET_Namespace) match {
        case Some(nextNS) => descentToNamespace(nextNS.asInstanceOf[NamespaceEntity], xs)
        case None => None
      }
    }
  }

  private def descentToTypedef(rootNS: NamespaceEntity, name: String, path: List[String]): Option[Type] = {
    descentToEntity(rootNS.asInstanceOf[NamespaceEntity], name, path, EEntityType.ET_Typedef) match {
      case Some(typedef) => Some(typedef.asInstanceOf[TypedefEntity].typedefType)
      case None => None
    }
  }

  private def findRootNamespace(qualifiedName: List[String]): Option[NamespaceEntity] = {
    qualifiedName match {
      case Nil => Some(namespaces.top)
      case "::" :: xs => Some(defaultNamespace)
      case rootName :: xs => lookupNamespaceInCurrentAndUp(rootName)
    }
  }

  override def resolveType(name: String, qualifiedSpecifier: List[String]): Option[Type] = {
    if (qualifiedSpecifier.isEmpty)
      resolveType(name) match {
        case Some(t) => return Some(t)
        case _ =>
      }

    findRootNamespace(qualifiedSpecifier) match {
      case Some(rootNS) => descentToTypedef(rootNS, name, qualifiedSpecifier.safeTail)
      case None => None
    }
  }

  def doStaticAssert(expr: Expression, msg: String): Unit = {
    expr match {
      case IdExpression(name, entity) => doStaticAssert(entity, msg)
      case _ => throw new Exception("Haha")
    }
  }

  private def doStaticAssert(entity: NamedEntity, msg: String): Unit = {
    entity match {
      case variable: VariableEntity =>
        val isVarConst = variable.varType match {
          case CompoundType(modifier, bound, subType) if modifier == TM_Const || modifier == TM_ConstVolatile => true
          case _ => false
        }

        if (!variable.specifiers.isConstExpr && !isVarConst)
          throw new SemanticError("static_assert on non-constant expression")

      case _ => throw new Exception(s"Don't know how to handle: $entity")
    }
  }
}
