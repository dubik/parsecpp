package org.dubikdev.cpp.semantic

import scala.collection.immutable.HashMap
import EFundamentalType._
import ETypeModifier._
import scala.collection.mutable.ListBuffer
import org.dubikdev.cpp.lexer.{Token, ETokenType}

object FundamentalTypes {
  val typesMap = HashMap(
    FT_SIGNED_CHAR -> "signed char",
    FT_SHORT_INT -> "short int",
    FT_INT -> "int",
    FT_LONG_INT -> "long int",
    FT_LONG_LONG_INT -> "long long int",
    FT_UNSIGNED_CHAR -> "unsigned char",
    FT_UNSIGNED_SHORT_INT -> "unsigned short int",
    FT_UNSIGNED_INT -> "unsigned int",
    FT_UNSIGNED_LONG_INT -> "unsigned long int",
    FT_UNSIGNED_LONG_LONG_INT -> "unsigned long long int",
    FT_WCHAR_T -> "wchar_t",
    FT_CHAR -> "char",
    FT_CHAR16_T -> "char16_t",
    FT_CHAR32_T -> "char32_t",
    FT_BOOL -> "bool",
    FT_FLOAT -> "float",
    FT_DOUBLE -> "double",
    FT_LONG_DOUBLE -> "long double",
    FT_VOID -> "void",
    FT_NULLPTR_T -> "nullptr_t",
    FT_SIGNED_CHAR -> "signed char",
    FT_UNSIGNED_INT -> "unsigned int",
    FT_SIGNED_INT -> "signed int",
    FT_UNSIGNED_SHORT_INT -> "unsigned short int",
    FT_UNSIGNED_SHORT -> "unsigned short",
    FT_UNSIGNED_LONG_INT -> "unsigned long int",
    FT_UNSIGNED_LONG -> "unsigned long",
    FT_UNSIGNED_LONG_LONG -> "unsigned long long",
    FT_SIGNED_LONG_INT -> "signed long int",
    FT_SIGNED_LONG -> "signed long",
    FT_SIGNED_LONG_LONG_INT -> "signed long long int",
    FT_SIGNED_LONG_LONG -> "signed long long",
    FT_LONG_LONG_INT -> "long long int",
    FT_LONG_INT -> "long int",
    FT_SIGNED_SHORT_INT -> "signed short int",
    FT_SIGNED_SHORT -> "signed short",
    FT_SHORT_INT -> "short int",
    FT_LONG_DOUBLE -> "long double",
    FT_UNSIGNED -> "unsigned",
    FT_SIGNED -> "signed",
    FT_LONG -> "long",
    FT_SHORT -> "short"
  )

  val typesSizeMap = HashMap(
    FT_SIGNED_CHAR -> 1,
    FT_SHORT_INT -> 2,
    FT_INT -> 4,
    FT_LONG_INT -> 8,
    FT_LONG_LONG_INT -> 8,
    FT_UNSIGNED_CHAR -> 1,
    FT_UNSIGNED_SHORT_INT -> 2,
    FT_UNSIGNED_INT -> 4,
    FT_UNSIGNED_LONG_INT -> 8,
    FT_UNSIGNED_LONG_LONG_INT -> 8,
    FT_WCHAR_T -> 4,
    FT_CHAR -> 1,
    FT_CHAR16_T -> 2,
    FT_CHAR32_T -> 4,
    FT_BOOL -> 1,
    FT_FLOAT -> 4,
    FT_DOUBLE -> 8,
    FT_LONG_DOUBLE -> 16,
    FT_NULLPTR_T -> 8
  )

  val str2TypeMap = typesMap.map(_.swap)

  val VoidType = new FundamentalType(FT_VOID)
}

object TypeModifiers {
  val modifiersMap = HashMap(
    TM_Pointer -> "pointer to",
    TM_LValueRef -> "lvalue-reference to",
    TM_RValueRef -> "rvalue-reference to",
    TM_Const -> "const",
    TM_Volatile -> "volatile",
    TM_ArrayOfUnknownBound -> "array of unknown bound of",
    TM_Array -> "array of"
  )

  val op2ModifierMap = HashMap(
    ETokenType.OP_STAR -> TM_Pointer,
    ETokenType.OP_AMP -> TM_LValueRef,
    ETokenType.OP_LAND -> TM_RValueRef
  )
}

object Type {
  def isRef(t: Type): Boolean =
    t match {
      case CompoundType(TM_LValueRef, _, _) => true
      case _ => false
    }

  def getEnclosingType(t: Type): Type =
    t match {
      case CompoundType(_, _, subType) => subType
      case _ => t
    }
}

trait Type {
  def isFundamentalType: Boolean = false

  def isCompoundType: Boolean = false

  def isFunctionType: Boolean = false
}

case class FundamentalType(fundamentalType: EFundamentalType) extends Type {
  override def toString = FundamentalTypes.typesMap(fundamentalType)

  override def isFundamentalType = true

  override def equals(obj: Any): Boolean =
    obj match {
      case that: FundamentalType => fundamentalType == that.fundamentalType
      case _ => false
    }
}

case class CompoundType(modifier: ETypeModifier, bound: String, subType: Type) extends Type {
  def this(modifier: ETypeModifier, subType: Type) = this(modifier, "", subType)

  override def toString = {
    if (modifier == TM_Array) {
      s"${TypeModifiers.modifiersMap(modifier)} $bound ${subType.toString}"
    } else {
      s"${TypeModifiers.modifiersMap(modifier)} ${subType.toString}"
    }
  }

  override def isCompoundType: Boolean = true

  override def equals(obj: Any): Boolean =
    obj match {
      case that: CompoundType => modifier == that.modifier && subType == that.subType && bound == that.bound
      case _ => false
    }
}

case class FunctionType(var returnType: Type, val vaargs: Boolean) extends Type {
  private val params: ListBuffer[Type] = ListBuffer[Type]()
  private var voidParam = false

  def this(returnType: Type) = this(returnType, false)

  def addParam(paramType: Type): Unit = {
    if (voidParam)
      throw new Exception("'void' must be the first and only parameter if specified")

    if (paramType != FundamentalTypes.VoidType)
      params += paramType
    else
      voidParam = true
  }

  def setReturn(retType: Type) = returnType = retType

  override def toString = {
    var paramsStrList = params.map(_.toString)
    if (vaargs)
      paramsStrList += "..."

    var paramsStr = paramsStrList.mkString(", ")
    paramsStr = s" of ($paramsStr)"

    s"function$paramsStr returning $returnType"
  }

  override def isFunctionType: Boolean = true

  override def equals(obj: Any): Boolean =
    obj match {
      case that: FunctionType => returnType == that.returnType && params == that.params && vaargs == that.vaargs
      case _ => false
    }
}
