package org.dubikdev.cpp.semantic

import org.dubikdev.cpp.ast.{IdExpression, LiteralExpression, Expression}
import org.dubikdev.cpp.abi.Alignment
import scala.collection.mutable.ListBuffer
import EFundamentalType._
import java.nio.{ByteOrder, ByteBuffer}
import scala.collection.mutable

import ETypeModifier._

trait Initializer {
  private var imageOffset: Int = 0

  def setOffset(newOffset: Int) = imageOffset = newOffset

  def offset(): Int = imageOffset

  def isConst: Boolean = false

  def isPRvalue: Boolean = false

  def toBytes(t: Type, offsetMap: mutable.HashMap[NamedEntity, Int]): List[Byte]

  def isValidInitializer = false
}

class CopyInitializer(val expression: Expression) extends Initializer {
  override def equals(obj: Any): Boolean =
    obj match {
      case that: CopyInitializer => expression == that.expression
      case _ => false
    }

  override def isValidInitializer = true

  override def toBytes(leftVarType: Type, offsetMap: mutable.HashMap[NamedEntity, Int]): List[Byte] = {
    expression match {
      case LiteralExpression(literal) => toBytes(leftVarType, literal)

      case IdExpression(qName, rightEntity) =>
        assert(offsetMap.contains(rightEntity), "Offset map should contain entity, because one pass was made already")
        val offset = offsetMap(rightEntity)

        rightEntity match {
          case f: FunctionEntity => toBytes(FT_UNSIGNED_LONG_INT, offset.toString)
          case rightVar: VariableEntity => toBytes(leftVarType, rightVar, offsetMap)
          case _ => throw new Exception(s"Doesn't know how to handle entity $rightEntity")
        }

      case _ => throw new Exception("Unknown expresion")
    }
  }

  private def findOffset(leftType: Type, rightEntity: Entity, offsetMap: mutable.HashMap[NamedEntity, Int]): String = {
    rightEntity match {
      case f: FunctionEntity => offsetMap(f).toString
      case rightVar: VariableEntity =>
        if (Type.isRef(leftType)) {
          rightVar.initializer.asInstanceOf[CopyInitializer].expression match {
            case LiteralExpression(_) => offsetMap(rightVar).toString
            case IdExpression(_, rightRightEntity) => findOffset(leftType, rightRightEntity, offsetMap)
          }
        } else {
          offsetMap(rightVar).toString
        }
    }
  }

  private def toBytes(leftVarType: Type, rightVar: VariableEntity, offsetMap: mutable.HashMap[NamedEntity, Int]): List[Byte] = {
    leftVarType match {
      case CompoundType(TM_LValueRef, _, _) =>
        val offset = findOffset(leftVarType, rightVar, offsetMap)
        toBytes(FT_UNSIGNED_LONG_INT, offset.toString)
      case _ => {
        rightVar.varType match {
          case CompoundType(TM_Array, bound, subType) => toBytes(FT_UNSIGNED_LONG_INT, findOffset(leftVarType, rightVar, offsetMap))
          case CompoundType(TM_Const, bound, subType) => {
            // TODO this is most likely wrong
            rightVar.initializer match {
              case c: CopyInitializer => {
                val initVal = ConstantExpressionEvaluator.calculate(c.expression)
                toBytes(subType, initVal)
              }
              case _ => throw new Exception(s"Don't know how to handle ${rightVar.varType}")
            }
          }

          case _ => throw new Exception(s"Don't know how to handle ${rightVar.varType}")
        }
      }
    }
  }

  private def toBytes(t: Type, literal: String): List[Byte] = {
    t match {
      case FundamentalType(fundType) => toBytes(fundType, literal)
      case CompoundType(modifier, bound, subType) =>
        if (List(TM_Const, TM_ConstVolatile, TM_Volatile).contains(modifier)) {
          toBytes(subType, literal)
        } else {
          throw new Exception(s"Don't know how to handle $modifier")
        }

      case _ => throw new Exception(s"Unknown type $t")
    }
  }

  private def toBytes(fundType: EFundamentalType, literal: String): List[Byte] = {
    val buffer = ByteBuffer.allocate(Alignment.typeSize(fundType)).order(ByteOrder.LITTLE_ENDIAN)

    fundType match {
      case FT_INT => buffer.putInt(literal.toInt)
      case FT_UNSIGNED_LONG_INT => buffer.putLong(literal.toLong)
      case _ => throw new Exception("Not handled")
    }

    buffer.array().toList
  }
}

class NoInitializer extends Initializer {
  override def equals(obj: Any): Boolean =
    obj.isInstanceOf[NoInitializer]

  override def toBytes(t: Type, offsetMap: mutable.HashMap[NamedEntity, Int]): List[Byte] = {
    val size = Alignment.typeSize(t)
    val out = ListBuffer[Byte]()
    0.until(size).foreach((index) => out += 0)
    out.toList
  }

  override def isValidInitializer = false
}
