package org.dubikdev.cpp.semantic;

import org.dubikdev.cpp.lexer.ETokenType;
import org.dubikdev.cpp.lexer.Token;

import java.util.HashMap;
import java.util.Map;

public enum EDeclSpecifier {
    DS_TypeDef(Token.kw(ETokenType.KW_TYPEDEF).getSource()),
    DS_Static(Token.kw(ETokenType.KW_STATIC).getSource()),
    DS_Extern(Token.kw(ETokenType.KW_EXTERN).getSource()),
    DS_ThreadLocal(Token.kw(ETokenType.KW_THREAD_LOCAL).getSource()),
    DS_ConstExpr(Token.kw(ETokenType.KW_CONSTEXPR).getSource()),
    DS_Inline(Token.kw(ETokenType.KW_INLINE).getSource());

    private String keyword;
    static private Map<String, EDeclSpecifier> strToDeclSpecifierMap = new HashMap<String, EDeclSpecifier>();

    static {
        for (EDeclSpecifier specifier : EDeclSpecifier.values()) {
            strToDeclSpecifierMap.put(specifier.toString(), specifier);
        }
    }

    EDeclSpecifier(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public String toString() {
        return keyword;
    }

    public static EDeclSpecifier findSpecifier(String tokenSpecifierType) {
        return strToDeclSpecifierMap.get(tokenSpecifierType);
    }
}
