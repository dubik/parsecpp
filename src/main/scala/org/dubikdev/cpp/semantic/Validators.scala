package org.dubikdev.cpp.semantic

import ETypeModifier._

object Validators {
  def validateVariable(entity: VariableEntity) = {
    checkIfThereIsNotInitializerForConstVariable(entity)
    checkIfLValueIsNotInitialized(entity)
  }

  private def checkIfThereIsNotInitializerForConstVariable(entity: VariableEntity) = {
    if (!entity.initializer.isValidInitializer && !entity.specifiers.isExtern) {
      throwIfTypeRequireInitializer(entity.varType, entity.varType.toString)
    }
  }

  private def throwIfTypeRequireInitializer(t: Type, diagTypeString: String): Unit = {
    t match {
      case CompoundType(modifier, bound, subType)
        if modifier == TM_Const || modifier == TM_ConstVolatile => throw new SemanticError(s"default initialization of an object of const type '$diagTypeString'")
      case CompoundType(TM_Array, bound, subType) => throwIfTypeRequireInitializer(subType, diagTypeString)
      case _ =>
    }
  }

  private def checkIfLValueIsNotInitialized(entity: VariableEntity) = {
    entity.varType match {
      case CompoundType(modifier, bound, subType) if modifier == TM_LValueRef && !entity.initializer.isValidInitializer =>
        throw new SemanticError(s"declaration of reference variable '${entity.name}' requires an initializer")
      case _ =>
    }
  }
}
