package org.dubikdev.cpp.semantic;

public enum ETypeModifier {
    TM_Const,
    TM_Volatile,
    TM_ConstVolatile,
    TM_Pointer,
    TM_LValueRef,
    TM_RValueRef,
    TM_ArrayOfUnknownBound,
    TM_Array,
    TM_FuncOfUnknownBound,
    TM_Func
}
