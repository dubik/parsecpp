package org.dubikdev.cpp.utils

object CollectionsHelper {

  implicit class TraversableHelper[T](list: Traversable[T]) {
    def existsEx[R](func: T => Option[R]): Option[R] = {
      val iter = list.toIterator
      var v: Option[R] = None
      while (iter.hasNext && v.isEmpty) {
        v = func(iter.next())
      }
      v
    }

  }

  implicit class ListHelper[T](list: List[T]) {
    /**
     * Returns Nil is list is empty, otherwise tail
     * @return tail if list is not empty or <code>Nil</code>
     */
    def safeTail: List[T] = list match {
      case Nil => list
      case _ => list.tail
    }
  }

}
