package org.dubikdev.cpp.ast;

public enum EExpressionCategory {
    EA_PRValue,
    EA_LValue,
    EA_RValue,
    EA_XValue
}
