package org.dubikdev.cpp.ast

import org.dubikdev.cpp.semantic._
import scala.Some

import EExpressionCategory._
import EFundamentalType._

trait Expression {
  def naturalType: Type

  def valueCategory: EExpressionCategory
}

case class LiteralExpression(literal: String) extends Expression {
  def naturalType: Type = literal match {
    case "true" => new FundamentalType(FT_BOOL)
    case "false" => new FundamentalType(FT_BOOL)
    case "nullptr" => new FundamentalType(FT_UNSIGNED_LONG_INT)
    case _ => throw new Exception(s"Can't figure out natural type of $literal")
  }

  // TODO string literals should have lvalue
  def valueCategory: EExpressionCategory = EA_PRValue
}

case class IdExpression(name: QualifiedName, entity: NamedEntity) extends Expression {
  def naturalType: Type = entity match {
    case variable: VariableEntity => variable.varType
    case _ => throw new Exception(s"Can't figure out type of $entity")
  }

  def valueCategory: EExpressionCategory = EA_LValue
}

object IdExpression {
  /*
    ast is requied for qualified redeclaration cases like this:
    namespace A
    {
      const int y = 4;
      extern char k[];
    }

    char A::k[y];
   */
  def apply(qName: QualifiedName, context: DeclarationParsingContext, ast: DeclAst): IdExpression = {
    var expr: IdExpression = null
    if (ast != null && ast.nestedNameSpecifier().nonEmpty) {
      val someEntity = context.lookup(ast.nestedNameSpecifier(), qName.name, EEntityType.ET_Function, EEntityType.ET_Variable)
      expr = someEntity match {
        case Some(entity) => new IdExpression(qName, entity)
        case None => null
      }
    }

    if (expr == null) {
      val someEntity = context.lookup(qName.nestedNameSpecifier, qName.name, EEntityType.ET_Function, EEntityType.ET_Variable)
      expr = someEntity match {
        case Some(entity) => new IdExpression(qName, entity)
        case None => throw new SemanticError(s"Can't find ${qName.name}")
      }
    }

    expr
  }
}
