package org.dubikdev.cpp.ast

import org.dubikdev.cpp.lexer.{ETokenType, Token}
import scala.collection.mutable.ListBuffer
import scala.collection.mutable
import org.dubikdev.cpp.semantic._
import org.dubikdev.cpp.semantic.ETypeModifier._
import org.dubikdev.cpp.semantic.TypeModifiers._
import scala.Some
import org.dubikdev.cpp.semantic.FunctionType
import org.dubikdev.cpp.semantic.CompoundType
import org.dubikdev.cpp.semantic.FundamentalType

sealed abstract class DeclAstNode {

}

case class FuncAstNode() extends DeclAstNode {

}

case class FundamentalTypeAstNode(token: Token) extends DeclAstNode {

}

case class PtrOpAstNode(token: Token) extends DeclAstNode {

}

case class ParamsAstNode(list: List[Type], vaargs: Boolean) extends DeclAstNode {

}

case class ArrayAstNode(bound: Option[String]) extends DeclAstNode {

}

class DeclAst {
  type AstNodeList = ListBuffer[DeclAstNode]

  private val listStack: mutable.Stack[AstNodeList] = mutable.Stack()
  // Accumulates ast nodes and after commitParam is called it creates a param type out of it
  private val paramAccumStack: mutable.Stack[AstNodeList] = mutable.Stack()
  private val paramTypesStack: mutable.Stack[ListBuffer[Type]] = mutable.Stack()

  listStack.push(ListBuffer[DeclAstNode]())
  paramAccumStack.push(ListBuffer[DeclAstNode]())

  private var nameStr: String = ""

  private var nestedNameSpecifierList: List[String] = Nil

  def setName(str: String) = if (nameStr.isEmpty) nameStr = str

  def name() = nameStr

  def setNestedNameSpecifier(specifier: List[String]) = nestedNameSpecifierList = specifier

  def nestedNameSpecifier(): List[String] = nestedNameSpecifierList

  def addFunc(): Unit = {
    listStack.top += new FuncAstNode
  }

  def addLiteral(boundExpr: Option[Expression]): Unit = {
    val const = boundExpr match {
      case Some(x) => Some(ConstantExpressionEvaluator.calculate(x))
      case None => None
    }

    listStack.top += new ArrayAstNode(const)
  }

  def startParams(): Unit = {
    listStack.push(ListBuffer[DeclAstNode]())
    paramTypesStack.push(ListBuffer[Type]())
  }

  def endParams(vaargs: Boolean): Unit = {
    listStack.pop() // commitParam should be called before this
    val params = paramTypesStack.pop()
    listStack.top += new ParamsAstNode(params.toList, vaargs)
  }

  def startParam(): Unit = {
  }

  def discardParam(): Unit = {
  }

  def commitParam(declSpecifier: DeclSpecifier): Unit = {
    val paramAccum = listStack.top
    val paramType = createTypeRecursively(paramAccum.toList, declSpecifier)
    paramTypesStack.top += adjust(paramType)
    listStack.top.clear()
  }

  def addPtrOp(token: Token): Unit = {
    listStack.top += new PtrOpAstNode(token)
  }

  def createType(declSpecifier: DeclSpecifier): Type = {
    val nodes = listStack.top.toList
    makeSureThereIsNoReferenceToReference(nodes)

    createTypeRecursively(nodes, declSpecifier)
  }

  private def makeSureThereIsNoReferenceToReference(nodes: List[DeclAstNode]) = {
    val refNode = new PtrOpAstNode(Token.kw(ETokenType.OP_AMP))
    val iter = nodes.sliding(2)
    val indexOf = iter.indexOf(List(refNode, refNode))
    if (indexOf != -1)
      throw new SemanticError(s"'${name()}' declared as a reference to reference")
  }

  /*
    8.3.5.5 After determining the type of each parameter, any parameter of type “array of T” or “function returning T” is
    adjusted to be “pointer to T” or “pointer to function returning T,” respectively. After producing the list
    of parameter types, any top-level cv-qualiﬁers modifying a parameter type are deleted when forming the function type.
   */
  private def adjust(t: Type): Type = {
    t match {
      case t: CompoundType => t match {
        case CompoundType(TM_Array, bound, subType) => new CompoundType(TM_Pointer, subType)
        case CompoundType(TM_ArrayOfUnknownBound, bound, subType) => new CompoundType(TM_Pointer, subType)
        case CompoundType(TM_Const, bound, subType) => adjust(subType)
        case CompoundType(TM_Volatile, bound, subType) => adjust(subType)
        case CompoundType(TM_ConstVolatile, bound, subType) => adjust(subType)
        case _ => t
      }

      case f: FunctionType => new CompoundType(TM_Pointer, f)
      case _ => t
    }
  }

  private def createWrappedType(modifier: ETypeModifier, baseType: Type): Type = {
    baseType match {
      case CompoundType(TM_LValueRef, bound, subType) if modifier == TM_LValueRef || modifier == TM_RValueRef =>
        baseType

      case CompoundType(TM_RValueRef, bound, subType) =>
        if (modifier == TM_LValueRef)
          new CompoundType(TM_LValueRef, subType)
        else
          baseType

      case _ => new CompoundType(modifier, baseType)
    }
  }

  private def createTypeRecursively(list: List[DeclAstNode], declSpecifier: DeclSpecifier): Type = {
    if (list.isEmpty) {
      return declSpecifier.createBaseType()
    } else {
      val head = list.head

      head match {
        case PtrOpAstNode(token) =>
          val baseType = createTypeRecursively(list.tail, declSpecifier)
          val modifier = op2ModifierMap(token.getTokenType)

          baseType match {
            case CompoundType(mod, _, _) if mod == TM_LValueRef || mod == TM_RValueRef => createWrappedType(modifier, baseType)
            case CompoundType(mod, _, subType@CompoundType(subTypeMod, _, _))
              if (mod == TM_Const || mod == TM_Volatile || mod == TM_ConstVolatile) && (subTypeMod == TM_LValueRef || subTypeMod == TM_RValueRef) =>
              createWrappedType(modifier, subType)
            case _ => new CompoundType(modifier, baseType)
          }

        case FundamentalTypeAstNode(token) =>
          new FundamentalType(FundamentalTypes.str2TypeMap(token.getSource))

        case ArrayAstNode(bound) =>
          val baseType: Type = createTypeRecursively(list.tail, declSpecifier)
          bound match {
            case Some(v) => new CompoundType(TM_Array, v, baseType)
            case None => new CompoundType(TM_ArrayOfUnknownBound, baseType)
          }

        case funcAstNode: FuncAstNode =>
          val params = list.tail.head.asInstanceOf[ParamsAstNode]
          val funcType = new FunctionType(createTypeRecursively(list.tail.tail, declSpecifier), params.vaargs)
          params.list.foreach((t) => funcType.addParam(t))
          funcType

        case _ => throw new Exception("Not expected input")
      }
    }
  }
}