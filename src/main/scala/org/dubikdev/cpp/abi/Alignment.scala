package org.dubikdev.cpp.abi

import org.dubikdev.cpp.semantic._
import scala.collection.mutable
import org.dubikdev.cpp.semantic.CompoundType
import org.dubikdev.cpp.semantic.FundamentalType
import ETypeModifier._

object Alignment {
  def align(aligned: Int, out: mutable.Buffer[Byte]): Unit =
    while (out.size % aligned != 0)
      out += 0

  def typeSize(t: Type): Int = {
    t match {
      case CompoundType(modifier, bound, subType) if modifier == TM_Array => sizeOfArray(subType, bound.toInt)
      case _ => typeAlignment(t)
    }
  }

  private def sizeOfArray(t: Type, bound: Int): Int = {
    typeSize(t) * bound
  }

  def typeSize(fundType: EFundamentalType): Int = FundamentalTypes.typesSizeMap(fundType)

  def typeAlignment(t: Type): Int = {
    t match {
      case FundamentalType(x) => typeSize(x)
      case CompoundType(modifier, bound, subType) => modifierAlignment(modifier, subType)
      case FunctionType(returnType, vaargs) => throw new Exception("Don't know how to handle function type at the moment")
      case _ => throw new Exception(s"Don't know how to align $t")
    }
  }

  private def modifierAlignment(modifier: ETypeModifier, subType: Type): Int = {
    modifier match {
      case TM_Const => typeAlignment(subType)
      case TM_Volatile => typeAlignment(subType)
      case TM_ConstVolatile => typeAlignment(subType)
      case TM_LValueRef => 8
      case TM_RValueRef => 8
      case TM_Pointer => 8
      case TM_Array => typeAlignment(subType)
      case TM_Func => 4
      case _ => throw new Exception(s"Unhandled modifier: $modifier")
    }
  }
}
