package org.dubikdev.cpp.abi

import org.dubikdev.cpp.semantic._
import scala.collection.mutable.ListBuffer
import scala.collection.mutable


object PA8MockDumper {
  def apply(entities: List[NamedEntity]): List[Byte] = {
    val output = ListBuffer[Byte]()
    val entitiesOffsets = mutable.HashMap[NamedEntity, Int]()
    val firstPass = new PA8MockDumper(false, ListBuffer(), entitiesOffsets)
    firstPass.dump(entities)
    val secondPass = new PA8MockDumper(true, output, entitiesOffsets)
    secondPass.dump(entities)
    return output.toList
  }
}

class PA8MockDumper(val serializeEntities: Boolean, val out: ListBuffer[Byte], val entitiesOffsets: mutable.HashMap[NamedEntity, Int]) {
  def dump(entities: List[NamedEntity]): Unit = {
    dumpString("PA8")

    entities.foreach {
      case v: VariableEntity if !v.specifiers.isExtern => dump(v)
      case f: FunctionEntity => dump(f)
      case _ =>
    }
  }

  private def dump(entity: VariableEntity): Unit = {
    val alignment = Alignment.typeAlignment(entity.varType)
    Alignment.align(alignment, out)
    entity.initializer.setOffset(out.size)
    entitiesOffsets.put(entity, out.size)
    if (serializeEntities) {
      val bytes = entity.initializer.toBytes(entity.varType, entitiesOffsets)
      out ++= bytes
    } else {
      extendOutBy(alignment)
    }
  }

  private def extendOutBy(size: Int): Unit =
    for (x <- 1 to size)
      out += 0

  private def dump(entity: FunctionEntity): Unit = {
    Alignment.align(4, out)
    entitiesOffsets.put(entity, out.size)
    dumpString("fun")
  }

  private def dumpString(str: String) = {
    str.foreach(out += _.toByte)
    out += 0
  }
}
