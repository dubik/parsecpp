package org.dubikdev.cpp.apps

import java.io.{StringReader, File, PrintWriter}
import org.dubikdev.cpp.TokenStream
import org.dubikdev.cpp.lexer.CppLexer
import org.dubikdev.cpp.parser.DeclarationParser
import org.dubikdev.cpp.semantic._
import org.dubikdev.cpp.support.PA

class NSDeclVisitor(val out: PrintWriter) extends EntityVisitor {
  def visit(entity: VariableEntity): Unit = {
    val name = entity.name
    val typeStr = entity.varType.toString
    out.println(s"variable $name $typeStr")
  }

  def visit(entity: FunctionEntity): Unit = {
    val name = entity.name
    val typeStr = entity.funcType.toString
    out.println(s"function $name $typeStr")
  }

  def visit(entity: TypedefEntity): Unit = {
  }

  def visitEnter(entity: NamespaceEntity): Unit = {
    val name = entity.name
    if (name == "unnamed" || name.startsWith("__unique__")) {
      out.println("start unnamed namespace")
    } else {
      out.println(s"start namespace $name")
    }

    if (entity.inline)
      out.println("inline namespace")
  }

  def visitExit(entity: NamespaceEntity): Unit = {
    out.println("end namespace")
  }
}

object NSDecl {
  def main(args: Array[String]) = {
    PA.isPA7 = true

    if (args.size < 3 || args.head != "-o") {
      println("NSDecl -o <output> <files> ")
    } else {
      val outputFile = args.tail.head
      val inputFiles = args.tail.tail
      process(outputFile, inputFiles.toList)
    }
  }

  def process(outputFile: String, inputFiles: List[String]): Unit = {
    val out = new PrintWriter(new File(outputFile))
    out.println(s"${inputFiles.size} translation units")
    inputFiles.foreach(processFile(out, _))
    out.flush()
    out.close()
  }

  def processFile(out: PrintWriter, inputFile: String) = {
    out.println(s"start translation unit $inputFile")
    val content = scala.io.Source.fromFile(inputFile).mkString
    processContent(out, content)
    out.println("end translation unit")
  }

  def processContent(out: PrintWriter, content: String) = {
    val stream = new TokenStream(new CppLexer(new StringReader(content)))
    val parser = new DeclarationParser(stream, "unique_not_needed")
    val parseResult = parser.translation_unit()
    if (parseResult) {
      val context = parser.context
      val rootNamspace = context.defaultNamespace
      val visitor = new NSDeclVisitor(out)
      rootNamspace.accept(visitor)
    } else {
      out.println("Can't parse!")
    }
  }
}
