package org.dubikdev.cpp.apps

import java.io.{File, FileOutputStream, StringReader}
import org.dubikdev.cpp.TokenStream
import org.dubikdev.cpp.lexer.CppLexer
import org.dubikdev.cpp.parser.DeclarationParser
import org.dubikdev.cpp.semantic._
import org.dubikdev.cpp.linker.Linker
import org.dubikdev.cpp.abi.PA8MockDumper

object NSInit {
  def main(argv: Array[String]) = {
    if (argv.size < 3 || argv.head != "-o") {
      println("NSDecl -o <output> <files> ")
    } else {
      val outputFile = argv.tail.head
      val inputFiles = argv.tail.tail
      process(outputFile, inputFiles.toList)
    }
  }

  private def process(outputFile: String, inputFiles: List[String]): Unit = {
    val bytesList = dump(inputFiles)
    save(outputFile, bytesList)
  }

  private def dump(inputFiles: List[String]): List[Byte] = {
    val objectModels = inputFiles.map(processFile(_))
    val linkedProgram = Linker.linkObjectModels(objectModels)
    PA8MockDumper(linkedProgram)
  }

  private def save(outputFile: String, out: List[Byte]): Unit = {
    val outputStream = new FileOutputStream(outputFile)
    outputStream.write(out.toArray)
  }

  private def processFile(inputFile: String): NamespaceEntity = {
    val unique = createUnique(inputFile)
    val content = scala.io.Source.fromFile(inputFile).mkString
    processContent(content, unique)
  }

  def processContent(content: String, unique: String): NamespaceEntity = {
    val stream = new TokenStream(new CppLexer(new StringReader(content)))
    val parser = new DeclarationParser(stream, unique)
    val parseResult = parser.translation_unit()
    if (parseResult) {
      val context = parser.context
      context.defaultNamespace
      // rootNamspace.accept(dumper)
    } else {
      throw new Exception("Can't parse!")
    }
  }

  private def createUnique(inputFile: String): String = new File(inputFile).getAbsolutePath.replace('/', '_').replace('\\', '_')
}
