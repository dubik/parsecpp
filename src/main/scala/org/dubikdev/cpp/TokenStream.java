package org.dubikdev.cpp;

import org.dubikdev.cpp.lexer.CppLexer;
import org.dubikdev.cpp.lexer.ETokenType;
import org.dubikdev.cpp.lexer.Token;
import scala.collection.Iterator;
import scala.collection.Seq;

import java.io.IOException;
import java.util.Vector;

public class TokenStream {
    private Vector<Token> tokens = new Vector<Token>();
    private Vector<Integer> savedPos = new Vector<Integer>();

    private int pos;

    public TokenStream(CppLexer lexer) {
        readAllTokens(lexer);
    }

    private void readAllTokens(CppLexer lexer) {
        try {
            Token token = lexer.yylex();

            while (token != null) {
                tokens.add(token);
                token = lexer.yylex();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean hasNext() {
        return pos < tokens.size();
    }

    public Token next() {
        if (hasNext()) {
            return tokens.get(pos++);
        }

        return null;
    }

    public Token peek() {
        if (hasNext()) {
            return tokens.get(pos);
        }
        return null;
    }

    public boolean nextIs(Token token) {
        if (hasNext()) {
            Token nextToken = peek();
            if (nextToken != null && nextToken.equals(token)) {
                pos++;
                return true;
            }
        }

        return false;
    }

    public void skip(int numItems) {
        pos += numItems;
    }

    public void save() {
        savedPos.add(pos);
    }

    public void discard() {
        savedPos.remove(savedPos.size() - 1);
    }

    public void restore() {
        pos = savedPos.get(savedPos.size() - 1);
        discard();
    }

    public boolean require(ETokenType tokenType) {
        return nextIs(Token.kw(tokenType));
    }

    public boolean require(Seq<ETokenType> types) {
        return require(types.iterator());
    }

    public boolean require(Iterator<ETokenType> iter) {
        while (iter.hasNext()) {
            ETokenType tokenType = iter.next();
            if (require(tokenType))
                return true;
        }

        return false;
    }
}
