package org.dubikdev.cpp.linker

import org.dubikdev.cpp.semantic._
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import org.dubikdev.cpp.ast.{IdExpression, LiteralExpression}

object Linker {
  type QualifiedName = List[String]

  class LinkModelsVisitor extends EntityVisitor {
    var constLiteralIndex = 0

    val entities = ListBuffer[NamedEntity]()

    private val variables = mutable.HashMap[QualifiedName, VariableEntity]()
    val nonStatics: mutable.Set[QualifiedName] = mutable.Set()
    val statics: mutable.Set[QualifiedName] = mutable.Set()
    val externs = mutable.HashMap[QualifiedName, VariableEntity]()
    val functions = mutable.HashMap[QualifiedName, FunctionEntity]()
    val constLiterals = ListBuffer[NamedEntity]()

    private val namespaceStack = mutable.Stack[String]()

    /*
     Statics can be define in the same translation unit only, and the don't clash
     if they were define somewhere else
     */
    private def handleStaticVariable(entity: VariableEntity, fullName: QualifiedName) = {
      // static int k;
      // static int k;
      if (statics.contains(fullName))
        throw new SemanticError(s"Static variable ${entity.name} redefinition")

      // int k;
      // static int k;
      if (nonStatics.contains(fullName))
        throw new SemanticError(s"static declaration of ${entity.name} follows non static declaration of ${entity.name}")

      statics += fullName
      entities += entity
    }

    /*
      If extern met first time, it should create entity, and if it is met following times,
      it is ignored
     */
    private def handleExternVariable(entity: VariableEntity, fullName: QualifiedName) = {
      if (!variables.contains(fullName)) {
        externs.put(fullName, entity)
        entities += entity
      }
    }

    private def handleVariable(entity: VariableEntity, fullName: QualifiedName) = {
      var targetEntity = entity

      if (entity.nestedNameSpecifier.nonEmpty && namespaceStack.size > 1) {
        val nestedName: List[String] = List("unnamed") ::: entity.nestedNameSpecifier
        val entityQName = QualifiedName(entity.name, nestedName)
        val qName = QualifiedName(fullName)
        if (entityQName.nestedNameSpecifier != qName.nestedNameSpecifier)
          throw new SemanticError("qualified name not from enclosed namespace")
      }

      if (statics.contains(fullName))
        throw new SemanticError(s"non-static declaration of ${entity.name} follows static declaration of ${entity.name}")

      if (variables.contains(fullName))
        throw new SemanticError(s"declaration of ${entity.name} redefined")

      if (externs.contains(fullName)) {
        replaceExternWithVariable(entity, fullName)
        externs.remove(fullName)
      } else if (Type.isRef(entity.varType)) {
        val proxyAndLiteral = createLiteralIfVarIsReference(entity)
        entities += proxyAndLiteral._1
        targetEntity = proxyAndLiteral._1
        if (proxyAndLiteral._2 != null)
          constLiterals += proxyAndLiteral._2
      } else {
        entities += entity
      }

      variables.put(fullName, targetEntity)
    }

    private def createLiteralIfVarIsReference(entity: VariableEntity): (VariableEntity, VariableEntity) = {
      entity.initializer match {
        case entityInitializer: CopyInitializer => entityInitializer.expression match {
          case initExpr: LiteralExpression => {
            val constLiteralName: String = s"__literal_$constLiteralIndex"
            constLiteralIndex += 1

            val literalVariable = new VariableEntity(constLiteralName, Type.getEnclosingType(entity.varType), entityInitializer)
            val initializer = new CopyInitializer(new IdExpression(QualifiedName(constLiteralName, Nil), literalVariable))
            val proxyEntity = new VariableEntity(entity.name, entity.nestedNameSpecifier, entity.varType, entity.specifiers, initializer)
            (proxyEntity, literalVariable)
          }

          case _ => (entity, null)
        }
        case _ => (entity, null)
      }
    }

    private def replaceExternWithVariable(variable: VariableEntity, fullName: QualifiedName) = {
      val externDeclaration = externs(fullName)
      val indexOfExtern = entities.indexOf(externDeclaration)
      entities.update(indexOfExtern, variable)
    }

    override def visit(entity: VariableEntity): Unit = {
      val fullName = (entity.name :: namespaceStack.toList).reverse

      if (entity.specifiers.isStatic) {
        handleStaticVariable(entity, fullName)
      } else if (entity.specifiers.isExtern) {
        handleExternVariable(entity, fullName)
      } else {
        handleVariable(entity, fullName)
      }
    }

    override def visit(entity: FunctionEntity): Unit = {
      entities += entity
    }

    override def visit(entity: TypedefEntity): Unit = {
    }

    override def visitEnter(entity: NamespaceEntity): Unit = {
      namespaceStack.push(entity.name)
    }

    override def visitExit(entity: NamespaceEntity): Unit = {
      namespaceStack.pop()
    }

    override def isPA7 = false

    def getEntities: List[NamedEntity] = {
      val allEntities = ListBuffer[NamedEntity]()
      allEntities ++= entities
      allEntities ++= constLiterals

      allEntities.toList
    }
  }

  def linkObjectModels(objectModels: List[NamespaceEntity]): List[NamedEntity] = {
    val linkVisitor = new LinkModelsVisitor
    objectModels.foreach((model) => {
      model.accept(linkVisitor)
      linkVisitor.statics.clear()
    })
    linkVisitor.getEntities
  }
}
