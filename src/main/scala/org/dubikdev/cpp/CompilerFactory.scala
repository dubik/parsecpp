package org.dubikdev.cpp

import java.io.StringReader
import scala.collection.mutable.ListBuffer
import org.dubikdev.cpp.lexer.{CppLexer, Token}

object CompilerFactory {
  def tokenize(str: String) = {
    val lexer = new CppLexer(new StringReader(str))
    val res = ListBuffer[Token]()

    var token: Token = lexer.yylex()
    do {
      res.append(token)
      token = lexer.yylex()
    } while (token != null)

    res.toList
  }
}
