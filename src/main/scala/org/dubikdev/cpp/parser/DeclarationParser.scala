package org.dubikdev.cpp.parser

import org.dubikdev.cpp.TokenStream
import org.dubikdev.cpp.lexer.{Token, ETokenType}
import ETokenType._
import org.dubikdev.cpp.semantic._
import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks._
import org.dubikdev.cpp.ast.{IdExpression, LiteralExpression, Expression, DeclAst}
import scala.Some
import EDeclSpecifier._

class DeclarationParser(val stream: TokenStream, val unique: String) {
  type ParseFunc = () => Boolean
  type ProcessFunc = (Token) => Unit
  type SimpleProcessFunc = () => Unit

  val context = new DeclarationParsingContext(unique)
  var declaratorAst = new DeclAst

  private def many(func: ParseFunc): Boolean = {
    while (func()) {
    }

    true
  }

  private def one_or_many(func: () => Boolean) = {
    if (func()) {
      many(func)
      true
    } else {
      false
    }
  }

  private def one_or_many[A](func: (A) => Boolean, context: A) = {
    if (func(context)) {
      many(func, context)
      true
    } else {
      false
    }
  }

  private def many[A](func: (A) => Boolean, context: A): Boolean = {
    while (func(context)) {
    }
    true
  }

  private def require(tokenType: ETokenType) = stream.require(tokenType)

  private def require(types: ETokenType*) = stream.require(types.toList)

  private def executeIfMatch(func: ProcessFunc, tokenType: ETokenType) = {
    val token = stream.peek
    if (stream.require(tokenType)) {
      func(token)
      true
    } else {
      false
    }
  }

  private def executeIfMatch(func: SimpleProcessFunc, tokenType: ETokenType) = {
    if (stream.require(tokenType)) {
      func()
      true
    } else {
      false
    }
  }

  private def executeIfMatch(func: ProcessFunc, types: ETokenType*) = {
    val token = stream.peek
    if (stream.require(types.toList)) {
      func(token)
      true
    } else {
      false
    }
  }

  def translation_unit(): Boolean = {
    if (stream.hasNext)
      many(declaration)

    !stream.hasNext
  }

  def declaration() =
    namespace_definition() || simple_declaration() || function_definition() || namespace_alias_definition() ||
      using_declaration() || using_directive() || static_assert_declaration() || alias_declaration() || empty_declaration()

  def empty_declaration(): Boolean = require(OP_SEMICOLON)


  /*
    alias-declaration:
     KW_USING TT_IDENTIFIER OP_ASS type-id OP_SEMICOLON
  */
  def alias_declaration(): Boolean = {
    stream.save()
    if (require(KW_USING)) {
      val token = stream.peek()
      if (token != null && token.isId) {
        stream.next
        if (require(OP_ASS)) {
          declaratorAst = new DeclAst
          declaratorAst.setName(token.getSource)
          val declSpecifier = new DeclSpecifier
          if (type_id(declSpecifier) && require(OP_SEMICOLON)) {
            context.makeAlias(declaratorAst, declSpecifier)
            return true
          }
        }
      }
    }

    stream.restore()
    false
  }

  /*
  type-id:
    type-specifier-seq abstract-declarator?
  */
  def type_id(declSpecifier: DeclSpecifier): Boolean = {
    val parsed = one_or_many(type_specifier, declSpecifier)
    abstract_declarator()
    parsed
  }

  /*
   abstract-declarator:
    ptr-abstract-declarator
  */
  def abstract_declarator(): Boolean = {
    stream.save()

    if (ptr_abstract_declarator()) {
      stream.discard()
      true
    } else {
      stream.restore()
      false
    }
  }

  /*
   ptr-abstract-declarator:
    ptr-operator* noptr-abstract-declarator
    ptr-operator+
  */
  def ptr_abstract_declarator(): Boolean = {
    val accum = ListBuffer[Token]()
    many(ptr_operator, accum)
    if (noptr_abstract_declarator()) {
      true
    } else {
      !accum.isEmpty
    }
  }

  /*
   noptr-abstract-declarator:
    noptr-abstract-declarator-root noptr-declarator-suffix*
    noptr-declarator-suffix+
  */
  def noptr_abstract_declarator(): Boolean = {
    if (noptr_abstract_declarator_root()) {
      many(noptr_declarator_suffix)
      true
    } else {
      one_or_many(noptr_declarator_suffix)
    }
  }

  /*
   noptr-abstract-declarator-root:
    noptr-declarator-suffix
    OP_LPAREN ptr-abstract-declarator OP_RPAREN
  */
  def noptr_abstract_declarator_root(): Boolean = {
    stream.save()

    if (require(OP_LPAREN) && ptr_abstract_declarator() && require(OP_RPAREN)) {
      stream.discard()
      true
    } else {
      stream.restore()
      noptr_declarator_suffix()
    }
  }

  /*
  namespace-alias-definition:
    KW_NAMESPACE TT_IDENTIFIER OP_ASS qualified-namespace-specifier OP_SEMICOLON
  */
  def namespace_alias_definition(): Boolean = {
    stream.save()
    if (require(KW_NAMESPACE) && stream.hasNext && stream.peek.isId) {
      val token = stream.next
      if (require(OP_ASS)) {
        val qualifiedNs = qualified_namespace_specifier()
        if (!qualifiedNs.isEmpty) {
          if (require(OP_SEMICOLON)) {
            context.makeNamespaceAlias(token.getSource, qualifiedNs)
            stream.discard()
            return true
          } else {
            throw new Exception("; expected")
          }
        } else {
          throw new Exception("Expecting namespace name")
        }
      }
    }
    stream.restore()
    false
  }

  def qualified_namespace_specifier(): List[String] = {
    val nestedNameSpecifier = nested_name_specifier()
    if (stream.hasNext && stream.peek.isId) {
      val token = stream.next
      nestedNameSpecifier :+ token.getSource
    } else {
      throw new Exception("Expecting namespace name")
    }
  }

  /*
   using-declaration:
    KW_USING nested-name-specifier unqualified-id OP_SEMICOLON
  */
  def using_declaration(): Boolean = {
    stream.save()

    if (require(KW_USING)) {
      val nameSpec = nested_name_specifier()
      if (stream.hasNext && stream.peek().isId) {
        val unqualifiedIdToken = stream.next()
        if (require(OP_SEMICOLON)) {
          context.using(nameSpec, unqualifiedIdToken.getSource)
          stream.discard()
          return true
        }
      }
    }

    stream.restore()
    false
  }

  /*
   using-directive:
    KW_USING KW_NAMESPACE nested-name-specifier? TT_IDENTIFIER OP_SEMICOLON
  */
  def using_directive(): Boolean = {
    stream.save()

    if (require(KW_USING) && require(KW_NAMESPACE)) {
      val nestedNameSpecifier = nested_name_specifier()
      if (stream.hasNext && stream.peek().getTokenType == TY_ID) {
        val ident = stream.next().getSource
        if (require(OP_SEMICOLON)) {
          context.usingNamespace(nestedNameSpecifier :+ ident)
          stream.discard()
        }
        return true
      }
    }

    stream.restore()
    false
  }

  /*
   namespace-definition:
    KW_INLINE? KW_NAMESPACE TT_IDENTIFIER? OP_LBRACE namespace-body OP_RBRACE
  */
  def namespace_definition(): Boolean = {
    stream.save()

    var inline = false
    if (require(KW_INLINE)) {
      inline = true
    }

    if (require(KW_NAMESPACE)) {
      var unnamedNSName: String = null
      val namespaceNameToken = stream.peek()
      if (namespaceNameToken.getTokenType == TY_ID) {
        stream.next()
      }

      if (require(OP_LBRACE)) {
        if (namespaceNameToken.getTokenType == TY_ID)
          context.pushNamespace(namespaceNameToken.getSource, inline)
        else
          unnamedNSName = context.pushUnnamedNamespace(inline).name

        if (namespace_body() && require(OP_RBRACE)) {
          context.commitNamespace()

          if (unnamedNSName != null)
            context.usingNamespace(List(unnamedNSName))

          stream.discard()
          return true
        } else {
          context.discardNamespace()
        }
      }
    }

    stream.restore()
    false
  }

  /*
  namespace-body:
    declaration*
  */
  def namespace_body() = many(declaration)

  /*
    simple-declaration:
      decl-specifier-seq init-declarator-list OP_SEMICOLON
  */
  def simple_declaration(): Boolean = {
    stream.save()
    val declSpecifier = new DeclSpecifier
    if (decl_specifier_seq(declSpecifier) && init_declarator_list(declSpecifier) && require(OP_SEMICOLON)) {
      stream.discard()
      true
    }
    else {
      stream.restore()
      false
    }
  }

  /*
    decl-specifier-seq:
      decl-specifier+
  */
  def decl_specifier_seq(declSpecifier: DeclSpecifier) = one_or_many(decl_specifier, declSpecifier)

  /*
    decl-specifier:
      storage-class-specifier
      type-specifier
      KW_TYPEDEF
      KW_INLINE
     */
  def decl_specifier(declSpecifier: DeclSpecifier) =
    executeIfMatch(() => declSpecifier.setDeclSpecifier(DS_TypeDef), KW_TYPEDEF) ||
      executeIfMatch(() => declSpecifier.setDeclSpecifier(DS_ConstExpr), KW_CONSTEXPR) ||
      executeIfMatch(() => declSpecifier.setDeclSpecifier(DS_Inline), KW_INLINE) ||
      storage_class_specifier(declSpecifier) ||
      type_specifier(declSpecifier)

  /*
    storage-class-specifier:
      KW_STATIC
      KW_THREAD_LOCAL
      KW_EXTERN
    */
  def storage_class_specifier(declSpecifier: DeclSpecifier): Boolean = {
    val token = stream.peek
    if (require(KW_STATIC, KW_THREAD_LOCAL, KW_EXTERN)) {
      declSpecifier.setDeclSpecifier(EDeclSpecifier.findSpecifier(token.getSource))
      true
    } else {
      false
    }
  }

  /*
    type-specifier:
  	  simple-type-specifier
	    cv-qualifier

    simple-type-specifier:
	    KW_CHAR
	    KW_CHAR16_T
	    KW_CHAR32_T
	    KW_WCHAR_T
	    KW_BOOL
	    KW_SHORT
	    KW_INT
	    KW_LONG
	    KW_SIGNED
	    KW_UNSIGNED
	    KW_FLOAT
	    KW_DOUBLE
	    KW_VOID
	    nested-name-specifier? type-name

	  cv-qualifier:
	    KW_CONST
	    KW_VOLATILE
   */
  def type_specifier(declSpecifier: DeclSpecifier) = {
    var parsed = executeIfMatch((token) => declSpecifier.setTypeSpecifier(context.resolveType(token.getSource).get)
      , KW_CHAR, KW_CHAR16_T, KW_CHAR32_T,
      KW_WCHAR_T, KW_BOOL, KW_SHORT, KW_INT, KW_LONG, KW_SIGNED, KW_UNSIGNED,
      KW_FLOAT, KW_DOUBLE, KW_VOID, KW_UNSIGNED_CHAR,
      KW_SIGNED_CHAR, KW_UNSIGNED_INT, KW_SIGNED_INT, KW_UNSIGNED_SHORT_INT,
      KW_UNSIGNED_SHORT, KW_UNSIGNED_LONG_INT, KW_UNSIGNED_LONG, KW_UNSIGNED_LONG_LONG_INT,
      KW_UNSIGNED_LONG_LONG, KW_SIGNED_LONG_INT, KW_SIGNED_LONG, KW_SIGNED_LONG_LONG_INT,
      KW_SIGNED_LONG_LONG, KW_LONG_LONG_INT, KW_LONG_INT, KW_SIGNED_SHORT_INT,
      KW_SIGNED_SHORT, KW_SHORT_INT, KW_LONG_DOUBLE) || executeIfMatch(declSpecifier.setCVQualifier(_), KW_CONST, KW_VOLATILE)

    if (!parsed) {
      stream.save()

      val nestedNameSpecifier = nested_name_specifier()
      parsed = qualified_type_name(declSpecifier, nestedNameSpecifier)

      if (parsed)
        stream.discard()
      else
        stream.restore()
    }

    parsed
  }

  def qualified_type_name(declSpecifier: DeclSpecifier, nestedNameSpecifier: List[String]): Boolean = {
    val token = stream.peek()
    if (token != null && token.isId && !declSpecifier.isTypeSpecifierDefined) {
      context.resolveType(token.getSource, nestedNameSpecifier) match {
        case Some(x) => {
          declSpecifier.setTypeSpecifier(x)
          stream.next()
          true
        }

        case None => false
      }
    } else {
      false
    }
  }

  /*
  nested-name-specifier:
	  nested-name-specifier-root nested-name-specifier-suffix*

	nested-name-specifier-root:
	  OP_COLON2
	  namespace-name OP_COLON2

  nested-name-specifier-suffix:
    TT_IDENTIFIER OP_COLON2
 */
  def nested_name_specifier(): List[String] = {
    stream.save()

    val list = ListBuffer[String]()
    if (require(OP_COLON2)) {
      list += Token.kw(OP_COLON2).getSource
    } else {
      val token = stream.next()
      if (token != null && token.isId && require(OP_COLON2)) {
        list += token.getSource
      } else {
        stream.restore()
        return List[String]()
      }
    }

    stream.discard()

    breakable {
      stream.save()
      while (stream.hasNext) {
        val token = stream.next()
        if (token.isId && require(OP_COLON2)) {
          stream.discard()
          list += token.getSource
          stream.save()
        } else {
          stream.restore()
          break()
        }
      }
    }

    list.toList
  }

  /*
    declarator:
      ptr-declarator
  */
  def declarator() = ptr_declarator()

  /*
    ptr-declarator:
	    ptr-operator* noptr-declarator-root noptr-declarator-suffix*
   */
  def ptr_declarator(): Boolean = {
    val ptrOperators = ListBuffer[Token]()
    many(ptr_operator, ptrOperators)
    if (noptr_declarator_root()) {
      many(noptr_declarator_suffix)
    }

    ptrOperators.reverse.foreach(declaratorAst.addPtrOp(_))

    true
  }

  /*
    ptr-operator:
      OP_STAR cv-qualifier*
      OP_AMP
      OP_LAND
  */
  def ptr_operator(accum: ListBuffer[Token]): Boolean = {
    val token = stream.peek()
    if (require(OP_AMP, OP_LAND)) {
      accum += token
      true
    } else if (require(OP_STAR)) {
      accum += token
      var nextToken = stream.peek()
      while (require(KW_CONST, KW_VOLATILE)) {
        accum += nextToken
        nextToken = stream.peek()
      }
      true
    } else {
      false
    }
  }

  def cv_qualifier() = require(KW_CONST, KW_VOLATILE)

  /*
    noptr-declarator-root:
	    id-expression
	    OP_LPAREN ptr-declarator OP_RPAREN
   */
  def noptr_declarator_root() = {
    id_expression() match {
      case Some(x) =>
        declaratorAst.setName(x.name)
        declaratorAst.setNestedNameSpecifier(x.nestedNameSpecifier)
        true

      case None => require(OP_LPAREN) && ptr_declarator() && require(OP_RPAREN)
    }
  }

  /*
    parameters-and-qualifiers:
      OP_LPAREN parameter-declaration-clause OP_RPAREN
     */
  def parameters_and_qualifiers() = {
    require(OP_LPAREN) && parameter_declaration_clause() && require(OP_RPAREN)
  }

  /*
    parameter-declaration-clause:
      parameter-declaration-list? OP_DOTS?
      parameter-declaration-list OP_COMMA OP_DOTS
  */
  def parameter_declaration_clause(): Boolean = {
    declaratorAst.addFunc()
    declaratorAst.startParams()

    var vaargs = false

    if (parameter_declaration_list()) {
      if (require(OP_DOTS)) {
        vaargs = true
      } else if (require(OP_COMMA)) {
        if (require(OP_DOTS)) {
          vaargs = true
        } else {
          declaratorAst.endParams(vaargs = false)
          return false
        }
      }
    } else if (require(OP_DOTS)) {
      vaargs = true
    }

    declaratorAst.endParams(vaargs)
    true
  }

  /*
    parameter-declaration-list:
	    parameter-declaration (OP_COMMA parameter-declaration)*
   */
  def parameter_declaration_list(): Boolean = {
    if (parameter_declaration()) {
      while (require(OP_COMMA)) {
        if (!parameter_declaration()) {
          return false
        }
      }

      true
    } else {
      false
    }
  }

  /*
    parameter-declaration:
      decl-specifier-seq declarator
      decl-specifier-seq abstract-declarator?
    */
  def parameter_declaration() = {
    val declSpecifier = new DeclSpecifier
    declaratorAst.startParam()
    if (decl_specifier_seq(declSpecifier)) {
      if (declarator()) {
        true
      }
      declaratorAst.commitParam(declSpecifier)
      true
    } else {
      declaratorAst.discardParam()
      false
    }
  }

  /*
    noptr-declarator-suffix:
      parameters-and-qualifiers
      OP_LSQUARE constant-expression? OP_RSQUARE
     */
  def noptr_declarator_suffix(): Boolean = {
    if (!parameters_and_qualifiers()) {
      if (require(OP_LSQUARE)) {
        val expr = constant_expression(declaratorAst)
        declaratorAst.addLiteral(expr)
        if (require(OP_RSQUARE)) {
          true
        } else {
          false
        }
      } else {
        false
      }
    } else {
      true
    }
  }

  /*
    expression:
      KW_TRUE
      KW_FALSE
      KW_NULLPTR
      TT_LITERAL
      OP_LPAREN expression OP_RPAREN
      id-expression
   */
  def expression(ast: DeclAst): Option[Expression] = {
    val token = stream.peek()
    if (List(KW_TRUE, KW_FALSE, KW_NULLPTR, TY_LITERAL).contains(token.getTokenType)) {
      Some(new LiteralExpression(stream.next().getSource))
    } else if (require(OP_LPAREN)) {
      val expr = expression(ast)
      if (require(OP_RPAREN))
        expr
      else
        throw new Exception("Expecting closing parenthesis")
    } else {
      id_expression() match {
        case Some(qName) => Some(IdExpression(qName, context, ast))
        case _ => None
      }
    }
  }

  def constant_expression(ast: DeclAst): Option[Expression] = {
    expression(ast)
  }

  /*
    id-expression:
      unqualified-id
      qualified-id

    unqualified-id:
      TT_IDENTIFIER

    qualified-id:
      nested-name-specifier unqualified-id
   */
  def id_expression(): Option[QualifiedName] = {
    stream.save()

    val nestedNameSpecifier = nested_name_specifier()

    if (stream.peek.isId) {
      val token = stream.next
      stream.discard()
      Some(new QualifiedName(token.getSource, nestedNameSpecifier))
    } else {
      stream.restore()
      None
    }
  }

  /*
    init-declarator-list:
      init-declarator (OP_COMMA init-declarator)*
   */
  def init_declarator_list(declSpecifier: DeclSpecifier): Boolean = {
    declaratorAst = new DeclAst
    if (init_declarator(declSpecifier)) {
      context.addDeclaratorEntity(declaratorAst, declSpecifier)

      while (require(OP_COMMA)) {
        declaratorAst = new DeclAst
        if (!declarator()) {
          return false
        }

        context.addDeclaratorEntity(declaratorAst, declSpecifier)
      }
    }

    true
  }

  /*
    init-declarator:
      declarator initializer?
   */
  def init_declarator(declSpecifier: DeclSpecifier): Boolean = {
    if (declarator()) {
      initializer() match {
        case Some(init) => declSpecifier.setInitializer(init)
        case None => declSpecifier.setInitializer(new NoInitializer())
      }

      true
    } else {
      false
    }
  }

  /*
    initializer:
      OP_ASS expression
   */
  def initializer(): Option[Initializer] = {
    if (stream.require(OP_ASS)) {
      expression(declaratorAst) match {
        case Some(expr) => Some(new CopyInitializer(expr))
        case None => throw new Exception("Expecting expression after '='")
      }
    } else {
      None
    }
  }

  /*
  function-definition:
    decl-specifier-seq declarator function-body
  */
  def function_definition(): Boolean = {
    val declSpecifier = new DeclSpecifier
    decl_specifier_seq(declSpecifier) && declarator() && function_body()
  }

  /*
  function-body:
    OP_LBRACE OP_RBRACE
  */
  def function_body(): Boolean = {
    require(OP_LBRACE) && require(OP_RBRACE)
  }

  /*
  static_assert-declaration:
    KW_STATIC_ASSERT OP_LPAREN constant-expression OP_COMMA TT_LITERAL OP_RPAREN OP_SEMICOLON
  */
  def static_assert_declaration(): Boolean = {
    if (require(KW_STATIC_ASSERT)) {
      if (require(OP_LPAREN)) {
        val expr = constant_expression(null)
        if (expr.isDefined) {
          if (require(OP_COMMA)) {
            val literalToken = stream.next
            if (literalToken != null && literalToken.getTokenType == TY_LITERAL) {
              if (require(OP_RPAREN)) {
                if (require(OP_SEMICOLON)) {
                  context.doStaticAssert(expr.get, literalToken.getSource)
                  true
                } else {
                  throw new Exception("OP_SEMICOLON is expected after OP_RPARENT")
                }
              } else {
                throw new Exception("OP_RPAREN is expected after TT_LITERAL")
              }
            } else {
              throw new Exception("TT_LITERAL is expected after OP_COMMA")
            }
          } else {
            throw new Exception("OP_COMMA is expected after OP_LPAREN")
          }
        } else {
          throw new Exception("constant-expression is expected after OP_LPAREN")
        }
      } else {
        throw new Exception("OP_LPAREN is expected after KW_STATIC_ASSERT")
      }
    } else {
      false
    }
  }
}