name := "ParsingCpp"

version := "0.1"

scalaVersion := "2.10.3"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "1.9.2" % "test"
)

libraryDependencies += "com.novocode" % "junit-interface" % "0.10" % "test"